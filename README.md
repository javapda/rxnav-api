# rxnav-api #

## notes ##
* This project is continuing to be developed with new functionality added daily - be sure to check back often.
* Released [Drug Interaction API](https://rxnav.nlm.nih.gov/InteractionAPIs.html#) support _12/24/2018_.
* Released rximage support _12/16/2018_.
* Released [Rxterms](https://rxnav.nlm.nih.gov/RxTermsAPIs.html#) support.

## description ##

* promise-based api to interact with rxnav system by NLM and NIH
* NOTE: the links here are relative to the README on the [bitbucket page](https://bitbucket.org/javapda/rxnav-api/src#readme).
* [docs](./docs/README.md)
* [examples](./docs/examples/examples.md)

## supports ##

### rxnorm / rxclass / rxterms / drugInteractionApi ###
* to return *xml*, invoke new RxnavApi().xml() and then chain subsequent calls. (e.g. new RxnavApi().xml().restfulEndpointsList())
* by default, json is returned by the rxnav-api, but to explicitly return *json*, invoke new RxnavApi().json() and then chain subsequent calls. (e.g. new RxnavApi().json().restfulEndpointsList())

* [restfulEndpointsList](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_list)

## drugInteractionApi ##

* [main](https://rxnav.nlm.nih.gov/InteractionAPIs.html#)
* [/interaction](https://rxnav.nlm.nih.gov/InteractionAPIs.html#uLink=Interaction_REST_findDrugInteractions)
* [/list](https://rxnav.nlm.nih.gov/InteractionAPIs.html#uLink=Interaction_REST_findInteractionsFromList)
* [/sources](https://rxnav.nlm.nih.gov/InteractionAPIs.html#uLink=Interaction_REST_getInteractionSources)
* [/version](https://rxnav.nlm.nih.gov/InteractionAPIs.html#uLink=Interaction_REST_getVersion)


## rximage ##

* [search - search for medication images]
![atorvastatin image](https://rxpillimage.nlm.nih.gov/RxImage/image/images/gallery/300/00378-3950-05_RXNAVIMAGE10_D33DE98F.jpg "atorvastatin image")

## rxterms ##

* [allconcepts - allConcepts](https://rxnav.nlm.nih.gov/RxTermsAPIs.html#uLink=RxTerms_REST_getAllConcepts)
* [allinfo - rxnormTerms](https://rxnav.nlm.nih.gov/RxTermsAPIs.html#uLink=RxTerms_REST_getAllRxTermInfo)
* [name - rxnormDisplayName](https://rxnav.nlm.nih.gov/RxTermsAPIs.html#uLink=RxTerms_REST_getRxTermDisplayName)
* [version](https://rxnav.nlm.nih.gov/RxTermsAPIs.html#uLink=RxTerms_REST_getRxTermsVersion)

## rxclass ##

* [byId](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassById)
* [byName](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findClassByName)
* [classesTypeList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassTypes)
* [similar - classesSimilarToClass](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findSimilarClassesByClass)
* [similarByRxcuis - similarByRxnormList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_findSimilarClassesByDrugList)
* [allClasses - byClassTypeList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getAllClasses)
* [class/byRxcui - classesByRxnorm](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassByRxNormDrugId)
* [class/byDrugName - byDrugName](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassByRxNormDrugName)
* [classContext](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassContexts)
* [classGraph](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassGraph)
* [classMembers](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassMembers)
* [classTree](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassTree)
* [classTypes - classTypeList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getClassTypes)
* [relas - drug class relationTypeList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getRelas)
* [similarInfo](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSimilarityInformation)
* [relaSources - sourceTypeList](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSourcesOfDrugClassRelations)
* [spellingsuggestions](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSpellingSuggestions)

## rxnorm ##

* [allConceptsByTtyList](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getAllConceptsByTTY)
* [approximateTerm](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getApproximateMatch)
* [brands - brandsContainingIngredients](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_getMultiIngredBrand)
* [displayNames - retrieve names](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getDisplayTerms)
* [doesRxnormHaveProperty](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_filterByProperty)
* [drugs - search drug](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getDrugs)
* [idtypes - idTypeList](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getIdTypes)
* [ndcproperties - ndcProperties - given an ndc code find its properties](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_getNDCProperties)
* [ndcstatus - ndcStatus - given an NDC return its status](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getNDCStatus)
* [allhistoricalndcs - ndcsForRxnorm all NDC(s) for a given rxnorm](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getAllHistoricalNDCs)
* [propCategories - propertyCategoryList list the property categories](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getPropCategories)
* [propnames - propertyNameList list names of available properties](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getPropNames)
* [relatypes - relationTypeList list relationship types](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getRelaTypes)
* [related?rela - rxnormRelatedConceptListByRelationalAttributes](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_getRelatedByRelationship)
 * [related?tty - rxnormRelatedConceptListByTermTypes](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_getRelatedByType)
* [rxnormsForFdbGcnSeqno]()
* [rxnormsForFdbIngredientId]()
* [rxnormsForHcpcsCode]()
* [rxnormsForNdc]()
* [rxnormsForOtherVocabulary](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_findRxcuiById)
* [rxnormsForSnomedCode]()
* [rxnormNdcList](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getNDCs)
* [rxnormProperties](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getRxConceptProperties)
* [rxnormProperty](https://rxnav.nlm.nih.gov/RxNormAPIREST.html#label%3Afunctions=&uLink=RxNorm_REST_getRxProperty)
* [rxnormProprietaryInfo (requires UMLS service ticket)](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getProprietaryInformation)
* [rxnormStatus](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getRxcuiStatus)
* [sourcetypes - sourceTypeList list available vocabulary abbreviated source types](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getSourceTypes)
* [spellingsuggestions - spellingSuggestionList given a med term return possible spellings](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getSpellingSuggestions)
* [termtypes - termTypes list available term types](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getTermTypes)
* [version returns the RxNorm data set version](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getRxNormVersion)

## install / setup ##

```
 npm install rxnav-api --save
```

# Resources #

* [rxnav api](https://rxnav.nlm.nih.gov/APIsOverview.html)
* [rxnav endpoints](https://rxnav.nlm.nih.gov/REST/)
* [rximage api](https://lhncbc.nlm.nih.gov/rximage-api) | [detail](https://rxnav.nlm.nih.gov/RxImageAPIs.html#) | [parameters](https://rxnav.nlm.nih.gov/RxImageAPIParameters.html)
* [axios](https://github.com/axios/axios)
* [rxnav-api on bitbucket](https://bitbucket.org/javapda/rxnav-api)
* [ndc list search](https://ndclist.com/search)
* [umls-api](https://npmjs.org/package/umls-api)
* [momentjs](https://npmjs.org/package/moment)
* [nodemon](https://nodemon.io/)
* [jsonformatter online](https://jsonformatter.org/)

# glossary / terminology #

* tty - term type 

# disclaimer #

* The information accessed through this API is from a government provided site.  The liability for the use of this data is solely at the user's discretion.

# credit #

* RxIMAGE is a product of the National Library of Medicine.