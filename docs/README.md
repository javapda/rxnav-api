# docs #

* [examples](./examples/examples.md)

# development notes #

* while developing a test script (e.g. test-rxnav-api.js) it grows tiresome to continually run the script. So - if you install [nodemon](https://nodemon.io/) then you can simply run `nodemon test/test-rxnav-api.js` and every time you touch a related file it will re-run.
