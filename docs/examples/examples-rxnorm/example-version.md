# example: version #

## usage: version ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().version()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{"version":"03-Dec-2018"}
```