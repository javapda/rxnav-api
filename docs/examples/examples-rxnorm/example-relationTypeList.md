# example: relationTypeList #

## usage: relationTypeList ##

* [site](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getRelaTypes)

### query string ###

* n/a

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().relationTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "relationTypeList": {
    "relationType": [
      "consists_of",
      "constitutes",
      "contained_in",
      "contains",
      "dose_form_of",
      "form_of",
      "has_dose_form",
      "doseformgroup_of",
      "has_form",
      "has_ingredient",
      "has_ingredients",
      "has_part",
      "has_precise_ingredient",
      "has_quantified_form",
      "has_tradename",
      "has_doseformgroup",
      "ingredient_of",
      "ingredients_of",
      "inverse_isa",
      "isa",
      "part_of",
      "precise_ingredient_of",
      "quantified_form_of",
      "reformulated_to",
      "reformulation_of",
      "tradename_of"
    ]
  }
}
```