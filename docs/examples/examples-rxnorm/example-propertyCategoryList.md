# example: propertyCategoryList #

## usage: propertyCategoryList ##

* [site](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getPropCategories)

### query string ###

* n/a

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().propertyCategoryList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "propCategoryList": {
    "propCategory": [
      "ATTRIBUTES",
      "CODES",
      "NAMES",
      "SOURCES"
    ]
  }
}
```