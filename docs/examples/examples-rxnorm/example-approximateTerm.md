# example: approximateTerm #

## usage: approximateTerm ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

new RxnavApi()
.rxnorm()
.approximateTerm({term:'zocor 10 mg',option:1,maxEntries:3})
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))
```
### output ###
```
{
  "approximateGroup": {
    "inputTerm": "zocor 10 mg",
    "maxEntries": "3",
    "option": "1",
    "comment": "",
    "candidate": [
      {
        "rxcui": "104490",
        "rxaui": "2651386",
        "score": "75",
        "rank": "1"
      },
      {
        "rxcui": "563653",
        "rxaui": "2278986",
        "score": "75",
        "rank": "1"
      }
    ]
  }
}
```