# example: ndcStatus #

## usage: ndcStatus ##

* [site](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getNDCStatus)

### query string ###

* ndc - the National Drug Code (NDC). The function accepts 10 digit NDC forms of 4-4-2 (example: 0781-1506-10), 5-3-2 (example: 60429-324-77) and 5-4-1 (example: 11523-7020-1) and the CMS 11-digit NDC derivative (example: 16571043111). It does not accept partial forms or NDC notation containing asterisks.

* start - (optional) the starting date in the format "YYYYMM" (ex: 201408). This parameter is used in conjunction with an end date to provide an interval to return history data. If specified, an end date must also be specified. If not specified, all history data is returned.

* end - (optional) - the ending date in the format "YYYYMM" (ex: 201505). This parameter is used in conjuction with a start date to provide an interval to return history data. If not specified, all history data is returned.

history - (optional, default:0) 0 = include all history. 1 = include only lastest entry 

altpkg - (optional, default:0) 1 = provide an alternate NDC with a different packaging component if the NDC is not found. For example, if NDC=40985021002 is not found, but NDC=40985021024 exists, it will be provided, and the altNdc field will be set to "Y". 
0 = no alternate NDC will be provided.

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let ndc = '00143314501'
    let payload = {
        ndc:ndc,
        start: null,
        end: null,
        history: 0,
        altpkg: 0
    }

    new RxnavApi().rxnorm().ndcStatus(payload)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "ndcStatus": {
    "ndc11": "00143314501",
    "status": "ACTIVE",
    "active": "YES",
    "rxnormNdc": "YES",
    "rxcui": "1116191",
    "conceptName": "Ephedrine sulfate 25 MG Oral Capsule",
    "conceptStatus": "ACTIVE",
    "sourceList": {
      "sourceName": [
        "MTHSPL",
        "GS",
        "MMSL",
        "MMX",
        "NDDF",
        "RXNORM",
        "VANDF"
      ]
    },
    "altNdc": "N",
    "comment": "",
    "ndcHistory": [
      {
        "activeRxcui": "1116191",
        "originalRxcui": "1116191",
        "startDate": "201108",
        "endDate": "201812"
      },
      {
        "activeRxcui": "1116191",
        "originalRxcui": "198918",
        "startDate": "200706",
        "endDate": "201107"
      }
    ]
  }
}
```