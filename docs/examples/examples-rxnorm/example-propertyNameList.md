# example: propertyNameList #

## usage: propertyNameList ##

* [site]()

### query string ###

* n/a

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().propertyNameList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "propNameList": {
    "propName": [
      "ACTIVATED",
      "ANADA",
      "ANDA",
      "ATC",
      "AVAILABLE_STRENGTH",
      "Active_ingredient",
      "Active_moiety",
      "BLA",
      "BN_CARDINALITY",
      "BoSS",
      "CVX",
      "DRUGBANK",
      "Denominator_Units",
      "Denominator_Value",
      "GENERAL_CARDINALITY",
      "HUMAN_DRUG",
      "IN_EXPRESSED_FLAG",
      "MESH",
      "MMSL_CODE",
      "NADA",
      "NDA",
      "NUI",
      "Numerator_Units",
      "Numerator_Value",
      "ORIG_CODE",
      "ORIG_SOURCE",
      "PRESCRIBABLE",
      "Prescribable Synonym",
      "QUALITATIVE_DISTINCTION",
      "QUANTITY",
      "RxCUI",
      "RxNorm Name",
      "RxNorm Synonym",
      "SCHEDULE",
      "SNOMEDCT",
      "SPL_SET_ID",
      "STRENGTH",
      "Source",
      "TTY",
      "Tallman Synonym",
      "UMLSCUI",
      "UNII_CODE",
      "VET_DRUG",
      "VUID"
    ]
  }
}
```