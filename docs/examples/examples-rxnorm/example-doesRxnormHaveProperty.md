# example: doesRxnormHaveProperty #

## usage: doesRxnormHaveProperty ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let morphineRxnorm = 7052
    new RxnavApi().rxnorm().doesRxnormHaveProperty(morphineRxnorm,{propName:'TTY',propValues:['IN']})
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{"rxcui":"7052"}
```