# example: ndcsForRxnorm #

## usage: ndcsForRxnorm ##

* [site](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getAllHistoricalNDCs)

### query string ###

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let rxnorm = 1668240
    new RxnavApi().rxnorm().rxcui().ndcsForRxnorm(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "historicalNdcConcept": {
    "historicalNdcTime": [
      {
        "status": "indirect",
        "rxcui": "351772",
        "ndcTime": [
          {
            "ndc": [
              "00069040001"
            ],
            "startDate": "201401",
            "endDate": "201510"
          },
          {
            "ndc": [
              "00069040010"
            ],
            "startDate": "201401",
            "endDate": "201510"
          },
          {
            "ndc": [
              "00069315014"
            ],
            "startDate": "200706",
            "endDate": "201510"
          },
          {
            "ndc": [
              "00069315083"
            ],
            "startDate": "200706",
            "endDate": "201510"
          },
          {
            "ndc": [
              "00069315084"
            ],
            "startDate": "201304",
            "endDate": "201510"
          },
          {
            "ndc": [
              "54569468100"
            ],
            "startDate": "200706",
            "endDate": "201101"
          },
          {
            "ndc": [
              "54868452700"
            ],
            "startDate": "200810",
            "endDate": "201510"
          },
          {
            "ndc": [
              "55154271505"
            ],
            "startDate": "200706",
            "endDate": "201206"
          },
          {
            "ndc": [
              "61947315000"
            ],
            "startDate": "200708",
            "endDate": "201206"
          },
          {
            "ndc": [
              "61947315001"
            ],
            "startDate": "200706",
            "endDate": "201206"
          },
          {
            "ndc": [
              "61947315003"
            ],
            "startDate": "200706",
            "endDate": "201206"
          }
        ]
      },
      {
        "status": "direct",
        "rxcui": "1668240",
        "ndcTime": [
          {
            "ndc": [
              "00069040001"
            ],
            "startDate": "201511",
            "endDate": "201812"
          },
          {
            "ndc": [
              "00069040010"
            ],
            "startDate": "201511",
            "endDate": "201812"
          },
          {
            "ndc": [
              "00069315014"
            ],
            "startDate": "201511",
            "endDate": "201812"
          },
          {
            "ndc": [
              "00069315083"
            ],
            "startDate": "201511",
            "endDate": "201812"
          },
          {
            "ndc": [
              "00069315084"
            ],
            "startDate": "201511",
            "endDate": "201812"
          },
          {
            "ndc": [
              "54868452700"
            ],
            "startDate": "201511",
            "endDate": "201812"
          }
        ]
      }
    ]
  }
}
```