# example: brandsContainingIngredients #

## usage: brandsContainingIngredients ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().brandsContainingIngredients([8896,20610])
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "brandGroup": {
    "ingredientList": {
      "rxnormId": [
        "8896",
        "20610"
      ]
    },
    "conceptProperties": [
      {
        "rxcui": "900604",
        "name": "Aller-Tec D",
        "synonym": "",
        "tty": "BN",
        "language": "ENG",
        "suppress": "N",
        "umlscui": "C2741214"
      },
      {
        "rxcui": "978024",
        "name": "Cetiri-D",
        "synonym": "",
        "tty": "BN",
        "language": "ENG",
        "suppress": "N",
        "umlscui": "C2911895"
      },
      {
        "rxcui": "977496",
        "name": "Wal Zyr D",
        "synonym": "",
        "tty": "BN",
        "language": "ENG",
        "suppress": "N",
        "umlscui": "C2756952"
      },
      {
        "rxcui": "353102",
        "name": "Zyrtec-D",
        "synonym": "",
        "tty": "BN",
        "language": "ENG",
        "suppress": "N",
        "umlscui": "C1170740"
      }
    ]
  }
}
```