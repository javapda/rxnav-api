# example: idTypeList #

## usage: idTypeList ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().idTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "idTypeList": {
    "idName": [
      "AMPID",
      "ANADA",
      "ANDA",
      "ATC",
      "BLA",
      "CVX",
      "DRUGBANK",
      "GCN_SEQNO",
      "GFC",
      "HCPCS",
      "HIC_SEQN",
      "MESH",
      "MMSL_CODE",
      "NADA",
      "NDA",
      "NDC",
      "NUI",
      "SNOMEDCT",
      "SPL_SET_ID",
      "UMLSCUI",
      "UNII_CODE",
      "USP",
      "VUID"
    ]
  }
}
```