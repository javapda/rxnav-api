# example: sourceTypeList #

## usage: sourceTypeList ##

* [site](https://rxnav.nlm.nih.gov/RxNormAPIs.html#uLink=RxNorm_REST_getSourceTypes)

### query string ###

* n/a

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().sourceTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```
### output ###
```
{
  "sourceTypeList": {
    "sourceName": [
      "ATC",
      "CVX",
      "DRUGBANK",
      "GS",
      "MDDB",
      "MMSL",
      "MMX",
      "MSH",
      "MTHSPL",
      "NDDF",
      "NDFRT",
      "RXNORM",
      "SNOMEDCT_US",
      "USP",
      "VANDF"
    ]
  }
}
```