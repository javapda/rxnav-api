# example: drugs #
## usage: drugs ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

new RxnavApi()
.rxnorm()
.drugs('cymbalta')
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))
```
### output ###
```
{
  "drugGroup": {
    "name": "cymbalta",
    "conceptGroup": [
      {
        "tty": "BPCK"
      },
      {
        "tty": "SBD",
        "conceptProperties": [
          {
            "rxcui": "596928",
            "name": "duloxetine 20 MG Delayed Release Oral Capsule [Cymbalta]",
            "synonym": "Cymbalta 20 MG Delayed Release Oral Capsule",
            "tty": "SBD",
            "language": "ENG",
            "suppress": "N",
            "umlscui": "C1656295"
          },
          {
            "rxcui": "596932",
            "name": "duloxetine 30 MG Delayed Release Oral Capsule [Cymbalta]",
            "synonym": "Cymbalta 30 MG Delayed Release Oral Capsule",
            "tty": "SBD",
            "language": "ENG",
            "suppress": "N",
            "umlscui": "C1614249"
          },
          {
            "rxcui": "615186",
            "name": "duloxetine 60 MG Delayed Release Oral Capsule [Cymbalta]",
            "synonym": "Cymbalta 60 MG (as duloxetine HCl 67.3 MG) Delayed Release Oral Capsule",
            "tty": "SBD",
            "language": "ENG",
            "suppress": "N",
            "umlscui": "C1624617"
          }
        ]
      }
    ]
  }
}
```
