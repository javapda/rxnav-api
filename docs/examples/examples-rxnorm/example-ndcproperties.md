## usage: ndcProperties ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

new RxnavApi()
.rxnorm()
.ndcProperties('37000-653-04')
.then((ndcPropertiesInfo)=>{
    console.log(`${JSON.stringify(ndcPropertiesInfo)}`)
})
.catch((err)=>console.log(`err=${err}`))
```
### output ###
```
{
  "ndcPropertyList": {
    "ndcProperty": [
      {
        "ndcItem": "37000065304",
        "ndc9": "37000-653",
        "ndc10": "37000-653-04",
        "rxcui": "211915",
        "splSetIdItem": "f7e829bf-5365-4b18-bfed-3c723cb0e2f6",
        "packagingList": {
          "packaging": [
            "118 mL in 1 BOTTLE, PLASTIC"
          ]
        },
        "propertyConceptList": {
          "propertyConcept": [
            {
              "propName": "COLOR",
              "propValue": "C48328"
            },
            {
              "propName": "MARKETING_EFFECTIVE_TIME_HIGH",
              "propValue": "20190913"
            },
            {
              "propName": "LABELER",
              "propValue": "The Procter & Gamble Manufacturing Company"
            },
            {
              "propName": "MARKETING_EFFECTIVE_TIME_LOW",
              "propValue": "20040630"
            },
            {
              "propName": "COLORTEXT",
              "propValue": "pink"
            },
            {
              "propName": "DM_SPL_ID",
              "propValue": "306572"
            },
            {
              "propName": "MARKETING_STATUS",
              "propValue": "completed"
            },
            {
              "propName": "LABEL_TYPE",
              "propValue": "HUMAN OTC DRUG LABEL"
            },
            {
              "propName": "OTC_MONOGRAPH_FINAL",
              "propValue": "part335"
            },
            {
              "propName": "MARKETING_CATEGORY",
              "propValue": "OTC monograph final"
            }
          ]
        }
      }
    ]
  }
}
```