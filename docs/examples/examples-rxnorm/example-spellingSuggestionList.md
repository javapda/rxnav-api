
# example: spellingSuggestionList #

## usage: spellingSuggestionList ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().spellingSuggestionList('ambien')
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{
  "suggestionGroup": {
    "name": "ambien",
    "suggestionList": {
      "suggestion": [
        "Ambien",
        "Ambifed",
        "Amber",
        "algin",
        "Amarel",
        "Amrinon",
        "Amrix",
        "Adagen",
        "Amvisc",
        "Altren"
      ]
    }
  }
}
```