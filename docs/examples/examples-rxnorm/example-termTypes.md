# example: allConceptsByTermTypeList #

## usage: allConceptsByTermTypeList ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxnorm().termTypes()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{
  "termTypeList": {
    "termType": [
      "BN",
      "BPCK",
      "DF",
      "DFG",
      "GPCK",
      "IN",
      "MIN",
      "PIN",
      "SBD",
      "SBDC",
      "SBDF",
      "SBDG",
      "SCD",
      "SCDC",
      "SCDF",
      "SCDG"
    ]
  }
}
```