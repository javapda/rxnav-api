# example: allConceptsByTermTypeList #

## usage: allConceptsByTermTypeList ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

new RxnavApi()
.rxnorm()
.allConceptsByTermTypeList(['BN','BPCK'])
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))
```
### output ###
```
...lots of data 
```