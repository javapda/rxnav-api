# example: search #

## usage: search ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
const rximageApi = new RxnavApi().rximage()
const searchParams = rximageApi.createSearchCriteria()
    .name("atorvastatin")
    .limitNumberOfRecords(1)
    .resolution(300)
    .params()

rximageApi
.search(searchParams,'rxnav')
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))
```
### picture / image ###
![atorvastatin image](https://rxpillimage.nlm.nih.gov/RxImage/image/images/gallery/300/00378-3950-05_RXNAVIMAGE10_D33DE98F.jpg "atorvastatin image")


### output ###
```
{
  "replyStatus": {
    "success": true,
    "date": "2018-12-16 23:50:31 GMT",
    "imageCount": 1,
    "totalImageCount": 33,
    "matchedTerms": {
      "name": "atorvastatin"
    }
  },
  "nlmRxImages": [
    {
      "ndc11": "00378-3950-05",
      "part": 1,
      "relabelersNdc9": [
        {
          "@sourceNdc9": "00378-3950",
          "ndc9": [
            "51079-0208",
            "69189-3950"
          ]
        }
      ],
      "rxcui": 617312,
      "splSetId": "d0a0feb6-84f0-4693-a11f-eb4f7ae0a87d",
      "splRootId": "1e44b527-d47f-492d-a262-11cdffab19f0",
      "splVersion": 13,
      "acqDate": "01-02-2015",
      "name": "atorvastatin 10 MG Oral Tablet",
      "labeler": "Mylan Pharmaceuticals Inc.",
      "attribution": "National Library of Medicine | Lister Hill National Center for Biomedical Communications",
      "id": 185683551,
      "imageUrl": "https://rxpillimage.nlm.nih.gov/RxImage/image/images/gallery/300/00378-3950-05_RXNAVIMAGE10_D33DE98F.jpg",
      "imageSize": 22919
    }
  ]
}
```
