# example: similarByRxcuis #

## usage: similarByRxcuis ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    let rxnormList = ['7052', '7676', '7804', '23088']
    let sourceOfDrugRelations = 'DAILYMED'
    let relationship = 'has_EPC'
    let scoreType = 0  // optional; 0=default
    let numberOfResultsToReturn = 5
    let equivalenceThreshold = null // 0.0-1.0
    let inclusionThreshold = null // 0.0-1.0 [only relevant for scoreType=1-or-2]
    const payload = {
        rxcuis: rxnormList,
        relaSource: sourceOfDrugRelations,
        rela: relationship,
        scoreType,
        top: numberOfResultsToReturn,
        equivalenceThreshold,
        inclusionThreshold
    }
    new RxnavApi().json().rxclass().similarByRxnormList(payload)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```

### output ###
```
{
  "userInput": {
    "relaSource": "DAILYMED",
    "relas": "ALL",
    "rxcui": "7052 7676 7804 23088",
    "scoreType": "0",
    "top": "5"
  },
  "similarityMember": {
    "ingredientRxcui": [
      "23088",
      "7052",
      "7676",
      "7804"
    ],
    "rankClassConcept": [
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.1605654",
          "inclusionScore": "-0.63461536",
          "intersection": "3",
          "cardinality1": "4",
          "cardinality2": "15"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000175684",
            "className": "Full Opioid Agonists",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.14272481",
          "inclusionScore": "-0.65",
          "intersection": "3",
          "cardinality1": "4",
          "cardinality2": "17"
        },
        "drugClassConceptItem": {
          "rela": "has_epc",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000175690",
            "className": "Opioid Agonist",
            "classType": "EPC"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.111697674",
          "inclusionScore": "-0.675",
          "intersection": "3",
          "cardinality1": "4",
          "cardinality2": "22"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000000174",
            "className": "Opioid Agonists",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.08858781",
          "inclusionScore": "-0.6923077",
          "intersection": "3",
          "cardinality1": "4",
          "cardinality2": "28"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000000200",
            "className": "Opioid Receptor Interactions",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.014352215",
          "inclusionScore": "-0.74147725",
          "intersection": "3",
          "cardinality1": "4",
          "cardinality2": "178"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000000152",
            "className": "G-Protein-linked Receptor Interactions",
            "classType": "MOA"
          }
        }
      }
    ]
  }
}
```