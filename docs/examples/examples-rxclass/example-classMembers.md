# example: classMembers #

## usage: classMembers ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    let classId = 'N0000008638' // Decreased GnRH Secretion
    let source = 'DAILYMED'
    let relationship = 'has_PE'
    let optionalDirectIndirectFlag = 0 // both direct and indirect
    // optionalDirectIndirectFlag = 1 // direct only
    let termTypeList = ['IN','PIN']
    new RxnavApi().json().rxclass().classMembers(classId, source, relationship, termTypeList, optionalDirectIndirectFlag)
        .then((res) =>console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

```

### output ###
```
{
  "userInput": {
    "classId": "N0000008638",
    "trans": "0",
    "relaSource": "DAILYMED",
    "rela": "has_PE",
    "ttys": [
      "IN",
      "PIN"
    ]
  },
  "drugMemberGroup": {
    "drugMember": [
      {
        "minConcept": {
          "rxcui": "68147",
          "name": "cetrorelix",
          "tty": "IN"
        },
        "nodeAttr": [
          {
            "attrName": "SourceId",
            "attrValue": "OON1HFZ4BA"
          },
          {
            "attrName": "SourceName",
            "attrValue": "CETRORELIX"
          },
          {
            "attrName": "Relation",
            "attrValue": "DIRECT"
          }
        ]
      },
      {
        "minConcept": {
          "rxcui": "35825",
          "name": "ganirelix",
          "tty": "IN"
        },
        "nodeAttr": [
          {
            "attrName": "SourceId",
            "attrValue": "IX503L9WN0"
          },
          {
            "attrName": "SourceName",
            "attrValue": "GANIRELIX"
          },
          {
            "attrName": "Relation",
            "attrValue": "DIRECT"
          }
        ]
      }
    ]
  }
}
```