# example: similarInfo #

## usage: similarInfo ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    const drugClass1 = {
        classId:'N02AA', // natural opium alkaloids
        relaSource : 'ATC',
        rela : null
    }
    const drugClass2 = {
        classId:'N0000008072', // morphine derivatives
        relaSource : 'NDFRT',
        rela : 'has_ingredient' 
    }
    new RxnavApi().json().rxclass().similarInfo(drugClass1,drugClass2)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

```

### output ###
* NOTE: suspect something wrong on the server - the results shown here do not match the [_expected results on the NIH server_](https://rxnav.nlm.nih.gov/RxClassAPIs.html#uLink=RxClass_REST_getSimilarityInformation)
```
{
  "userInput": {
    "classId1": "N02AA",
    "classId2": "N0000008072",
    "relaSource1": "ATC",
    "relaSource2": "NDFRT",
    "rela1": "",
    "rela2": "has_ingredient"
  }
}
```