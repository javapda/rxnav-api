# example: byClassTypeList #

## usage: byClassTypeList ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
let classTypeList = ['MOA']

new RxnavApi()
.rxclass()
.byClassTypeList(classTypeList)
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))

```

### output ###
```
{
  "userInput": {
    "classTypes": "MOA"
  },
  "rxclassMinConceptList": {
    "rxclassMinConcept": [
      {
        "classId": "N0000000065",
        "className": "Physiochemical Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000067",
        "className": "HER Receptor Family Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000068",
        "className": "Parathormone Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000069",
        "className": "Calcium Channel Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000070",
        "className": "Angiotensin 2 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000071",
        "className": "Insulin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000073",
        "className": "Glycine Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000074",
        "className": "GABA A Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000075",
        "className": "Stool Bulking Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000076",
        "className": "Protease Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000077",
        "className": "Somatotropin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000078",
        "className": "Chloride Channel Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000079",
        "className": "Surfactant Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000080",
        "className": "Cholinesterase Reactivators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000081",
        "className": "Macrophage Colony Stimulating Factor Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000082",
        "className": "Immunologic Factor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000083",
        "className": "Leukotriene Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000084",
        "className": "Sex Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000085",
        "className": "Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000086",
        "className": "Complement 1 Inactivators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000087",
        "className": "Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000089",
        "className": "Plasminogen Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000090",
        "className": "Serotonin 5HT-3 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000092",
        "className": "Adrenergic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000093",
        "className": "Platelet-derived Growth Factor alpha Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000094",
        "className": "Mineralocorticoid Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000095",
        "className": "Serotonin 5HT-3 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000097",
        "className": "Cholinergic Muscarinic Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000098",
        "className": "Thymidylate Synthetase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000099",
        "className": "Adrenergic alpha-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000100",
        "className": "Estrogen Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000101",
        "className": "Cysteine Proteinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000102",
        "className": "Norepinephrine Uptake Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000103",
        "className": "Reverse Transcriptase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000104",
        "className": "Cholinergic Muscarinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000105",
        "className": "Neurotransmitter Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000106",
        "className": "Prostaglandin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000107",
        "className": "Antithrombins",
        "classType": "MOA"
      },
      {
        "classId": "N0000000108",
        "className": "Prostaglandin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000109",
        "className": "Serotonin Uptake Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000110",
        "className": "Glucocorticoid Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000111",
        "className": "Folic Acid Metabolism Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000112",
        "className": "Histamine Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000114",
        "className": "Dopamine Uptake Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000115",
        "className": "Progestational Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000116",
        "className": "GABA B Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000117",
        "className": "Dopamine Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000118",
        "className": "GABA A Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000119",
        "className": "Glycine Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000120",
        "className": "Nitric Oxide Donors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000121",
        "className": "Hydroxymethylglutaryl-CoA Reductase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000122",
        "className": "Adrenergic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000123",
        "className": "Melatonin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000124",
        "className": "Leukotriene Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000125",
        "className": "Cholinergic Muscarinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000126",
        "className": "5-alpha Reductase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000127",
        "className": "HIV Integrase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000128",
        "className": "Phosphodiesterase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000130",
        "className": "Serotonin Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000131",
        "className": "Prostaglandin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000132",
        "className": "Organic Anion Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000133",
        "className": "Enzyme Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000134",
        "className": "Lipoxygenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000135",
        "className": "Immunologic Adjuvants",
        "classType": "MOA"
      },
      {
        "classId": "N0000000136",
        "className": "Parathormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000137",
        "className": "IGF Type 1 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000138",
        "className": "GABA A Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000139",
        "className": "Mineralocorticoid Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000140",
        "className": "Calcium Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000141",
        "className": "Pituitary Hormones Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000142",
        "className": "Biological Macromolecular Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000143",
        "className": "Cholinergic Nicotinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000144",
        "className": "Iron Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000145",
        "className": "Estrogen Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000146",
        "className": "Androgen Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000147",
        "className": "Proton Pump Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000149",
        "className": "Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000150",
        "className": "Protein Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000151",
        "className": "Histamine H2 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000152",
        "className": "G-Protein-linked Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000153",
        "className": "Adrenergic Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000154",
        "className": "Opioid Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000155",
        "className": "Uncouplers",
        "classType": "MOA"
      },
      {
        "classId": "N0000000156",
        "className": "Chloride Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000157",
        "className": "Interferon Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000000158",
        "className": "Parathormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000159",
        "className": "Glucocorticoid Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000160",
        "className": "Cyclooxygenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000161",
        "className": "Adrenergic beta-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000162",
        "className": "Steroid Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000163",
        "className": "Enzyme Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000164",
        "className": "Vitamin K Epoxide Reductase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000165",
        "className": "Pituitary Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000166",
        "className": "alpha Glucosidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000167",
        "className": "Serotonin Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000168",
        "className": "Selective Estrogen Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000169",
        "className": "Serine Proteinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000170",
        "className": "Free Radical Scavenging Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000172",
        "className": "Steroid Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000173",
        "className": "Nitric Oxide Synthase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000174",
        "className": "Opioid Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000175",
        "className": "Cytokine Receptor Superfamily Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000176",
        "className": "Topoisomerase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000177",
        "className": "Cholinesterase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000178",
        "className": "Enzymatic Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000179",
        "className": "Alcohol Dehydrogenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000180",
        "className": "Sodium Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000181",
        "className": "Angiotensin-converting Enzyme Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000182",
        "className": "Steroid Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000183",
        "className": "Acetyl Aldehyde Dehydrogenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000184",
        "className": "Monoamine Oxidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000185",
        "className": "Progestational Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000186",
        "className": "Estrogen Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000187",
        "className": "Adenosine Deaminase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000188",
        "className": "Serotonin 5HT-3 Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000189",
        "className": "Platelet-derived Growth Factor beta Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000190",
        "className": "Histamine H1 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000191",
        "className": "Dihydrofolate Reductase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000192",
        "className": "Complement Inactivators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000193",
        "className": "Ion Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000194",
        "className": "Somatostatin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000195",
        "className": "Enkephalinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000196",
        "className": "GABA A Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000197",
        "className": "Trypsin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000198",
        "className": "Serotonin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000199",
        "className": "Cholinergic Nicotinic Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000200",
        "className": "Opioid Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000202",
        "className": "beta Lactamase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000203",
        "className": "Dopamine Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000204",
        "className": "GABA B Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000205",
        "className": "Radiopharmaceutical Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000206",
        "className": "Xanthine Oxidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000207",
        "className": "Histamine Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000208",
        "className": "Enzyme Reactivators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000209",
        "className": "Adrenergic alpha-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000211",
        "className": "Potassium Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000212",
        "className": "Ribonucleotide Reductase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000213",
        "className": "Receptor Protein Kinase Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000214",
        "className": "Sex Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000215",
        "className": "ATP-Binding Cassette (ABC) Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000216",
        "className": "Histamine Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000217",
        "className": "Somatotropin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000218",
        "className": "Somatostatin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000219",
        "className": "Dopamine Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000220",
        "className": "Electrolyte Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000221",
        "className": "Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000222",
        "className": "Thyroid Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000223",
        "className": "Mechanism of Action (MoA)",
        "classType": "MOA"
      },
      {
        "classId": "N0000000224",
        "className": "Chloride Channel Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000225",
        "className": "Insulin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000226",
        "className": "Acid-Base Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000227",
        "className": "Cholinergic Nicotinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000228",
        "className": "Structural Macromolecules",
        "classType": "MOA"
      },
      {
        "classId": "N0000000229",
        "className": "GABA B Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000230",
        "className": "Dopamine Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000231",
        "className": "Enzyme Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000233",
        "className": "Nucleic Acid Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000234",
        "className": "Norepinephrine Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000235",
        "className": "Carbonic Anhydrase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000236",
        "className": "Alkylating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000237",
        "className": "Thyroid Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000238",
        "className": "Sodium-Potassium Exchanging ATPase Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000239",
        "className": "P-Glycoprotein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000240",
        "className": "Insulin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000241",
        "className": "Tissue Plasminogen Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000242",
        "className": "Transcription Factor Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000000243",
        "className": "Androgen Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000244",
        "className": "Glycine Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000245",
        "className": "Adrenergic beta-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000246",
        "className": "HIV Protease Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000247",
        "className": "Biological Response Modifiers",
        "classType": "MOA"
      },
      {
        "classId": "N0000000249",
        "className": "Pituitary Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000250",
        "className": "Melatonin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000251",
        "className": "GABA B Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000252",
        "className": "GTP Phosphohydrolase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000254",
        "className": "Immunologic and Biological Factor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000255",
        "className": "Hormone Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000000256",
        "className": "Serotonin Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000000257",
        "className": "Hormone Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000000258",
        "className": "Integrase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000000259",
        "className": "Calcium Channel Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000008288",
        "className": "Cyclooxygenase 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009915",
        "className": "Unknown Cellular or Molecular Interaction",
        "classType": "MOA"
      },
      {
        "classId": "N0000009916",
        "className": "Lipase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009917",
        "className": "Adrenergic alpha1-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009918",
        "className": "Adrenergic alpha2-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009919",
        "className": "Adrenergic alpha1-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009920",
        "className": "Adrenergic alpha2-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009921",
        "className": "Adrenergic beta1-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009922",
        "className": "Adrenergic beta2-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009923",
        "className": "Adrenergic beta1-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009924",
        "className": "Adrenergic beta2-Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000009946",
        "className": "Para-Aminobenzoic Acid Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009947",
        "className": "Nucleoside Reverse Transcriptase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009948",
        "className": "Non-Nucleoside Reverse Transcriptase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009949",
        "className": "Antibody-Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009950",
        "className": "Tumor Necrosis Factor Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009953",
        "className": "Synaptic Fusion Complex Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009954",
        "className": "Cell Membrane Fusion Complex Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009955",
        "className": "Acetylcholine Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009956",
        "className": "Sterol Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009957",
        "className": "Ergosterol Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009958",
        "className": "Bone Surface Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000009959",
        "className": "Dephosphorylation Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009960",
        "className": "Antithrombin Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000009961",
        "className": "Platelet Activating Factor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009962",
        "className": "Platelet Glycoprotein IIb/IIIA Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009963",
        "className": "Thrombin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009983",
        "className": "Sulfate Donors",
        "classType": "MOA"
      },
      {
        "classId": "N0000009986",
        "className": "Alkalinizing Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000010215",
        "className": "Transpeptidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010217",
        "className": "Photoabsorption",
        "classType": "MOA"
      },
      {
        "classId": "N0000010218",
        "className": "Ultraviolet A Radiation Absorption",
        "classType": "MOA"
      },
      {
        "classId": "N0000010219",
        "className": "Ultraviolet B Radiation Absorption",
        "classType": "MOA"
      },
      {
        "classId": "N0000010226",
        "className": "Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010255",
        "className": "Cellular Structure Molecule Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010256",
        "className": "Tubulin Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010257",
        "className": "Imaging Contrast Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000010258",
        "className": "X-Ray Contrast Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000010259",
        "className": "Ultrasound Contrast Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000010260",
        "className": "beta Tubulin Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010261",
        "className": "Neurokinin 1 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010262",
        "className": "Neurokinin 1 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000010263",
        "className": "Tissue Plasminogen Activator Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010264",
        "className": "Plasmin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010265",
        "className": "Squalene Monooxygenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010266",
        "className": "Growth Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000010267",
        "className": "Growth Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000010274",
        "className": "Amylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010275",
        "className": "Glucagon Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010276",
        "className": "Iodide Peroxidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000010278",
        "className": "Interleukin 2 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010279",
        "className": "Interleukin 2 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000010280",
        "className": "Interleukin 2 Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000010281",
        "className": "CD3 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000010288",
        "className": "Osmotic Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000010334",
        "className": "Adenosine Diphosphate Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000011310",
        "className": "Aldosterone Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000011319",
        "className": "Estradiol Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000019992",
        "className": "Hypothalmic Hormone Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000019993",
        "className": "Hypothalamic Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000019994",
        "className": "Hypothalamic Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000019995",
        "className": "DNA Methyltransferase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020000",
        "className": "Receptor Tyrosine Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020001",
        "className": "Tyrosine Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020006",
        "className": "HER Receptor Family Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020007",
        "className": "Vascular Endothelial Growth Factor Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020008",
        "className": "HER2/Neu/cerbB2 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020009",
        "className": "Bcr-Abl Tyrosine Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020010",
        "className": "HER1 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020012",
        "className": "HER3 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020014",
        "className": "Ionotropic Glutamate Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020015",
        "className": "NMDA Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020016",
        "className": "AMPA Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020017",
        "className": "Kainate Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020018",
        "className": "Glucosyl Transferase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020019",
        "className": "Glucosylceramide Synthase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020023",
        "className": "Antibody-Surface Protein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000020024",
        "className": "Sclerosing Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020025",
        "className": "14-alpha Demethylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020026",
        "className": "Phosphodiesterase 5 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020027",
        "className": "Glucan Synthase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020028",
        "className": "Platelet-derived Growth Factor alpha Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020029",
        "className": "Platelet-derived Growth Factor beta Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020030",
        "className": "Platelet-derived Growth Factor Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020031",
        "className": "VEGFR1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020032",
        "className": "VEGFR2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020033",
        "className": "VEGFR3 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020034",
        "className": "Stem Cell Factor (KIT) Receptor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020036",
        "className": "Colony Stimulating Factor Receptor Type 1 (CSF-1R) Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020037",
        "className": "Neurotrophic Factor Receptor (RET) Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020044",
        "className": "Vasopressin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020057",
        "className": "Glucagon-like Peptide-1 (GLP-1) Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000020058",
        "className": "Glucagon-like Peptide-1 (GLP-1) Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020059",
        "className": "Glucagon-like Peptide-1 (GLP-1) Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020060",
        "className": "DNA Polymerase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020061",
        "className": "Receptor Activity Modifying Proteins",
        "classType": "MOA"
      },
      {
        "classId": "N0000020062",
        "className": "Receptor Activity Modifying Protein Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020063",
        "className": "Receptor Activity Modifying Protein Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020064",
        "className": "Amylin Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020065",
        "className": "L-Calcium Channel Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020066",
        "className": "T-Calcium Channel Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020067",
        "className": "Voltage-gated Calcium Channel Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020068",
        "className": "Prostacyclin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020069",
        "className": "N-Calcium Channel Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020070",
        "className": "Keratinocyte Growth Factor Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000020071",
        "className": "Keratinocyte Growth Factor Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020072",
        "className": "Keratinocyte Growth Factor Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020074",
        "className": "Phosphate Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020075",
        "className": "Copper Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020076",
        "className": "Plutonium Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020077",
        "className": "Secretin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000020078",
        "className": "Secretin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020079",
        "className": "Secretin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000020080",
        "className": "Calcium-Sensing Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000020081",
        "className": "Increased Calcium-sensing Receptor Sensitivity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020082",
        "className": "Decreased Calcium-sensing Receptor Sensitivity",
        "classType": "MOA"
      },
      {
        "classId": "N0000020084",
        "className": "Ubiquitin Ligase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020085",
        "className": "E3 Ubiquitin Ligase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020086",
        "className": "DNA Gyrase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020087",
        "className": "Topoisomerase 4 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020088",
        "className": "Topoisomerase 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000020089",
        "className": "Viral Envelope Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175071",
        "className": "Histone Deacetylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175074",
        "className": "Nucleoside Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175075",
        "className": "Proteasome Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175076",
        "className": "Protein Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175077",
        "className": "CD33-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175078",
        "className": "CD20-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175079",
        "className": "CD52-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175080",
        "className": "Aromatase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175082",
        "className": "Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175084",
        "className": "Gonadotropin Releasing Hormone Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175086",
        "className": "Phosphodiesterase 3 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175087",
        "className": "Urease Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175089",
        "className": "Calcium Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175355",
        "className": "Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175356",
        "className": "Ion Exchange Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175357",
        "className": "Potassium Ion Exchange Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175361",
        "className": "Catecholamine Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175362",
        "className": "Endothelin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175363",
        "className": "Endothelin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175364",
        "className": "Endothelin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175365",
        "className": "Bile-acid Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175368",
        "className": "Cholinergic Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175369",
        "className": "Cholinergic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175370",
        "className": "Cholinergic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175374",
        "className": "Peroxisome Proliferator-activated Receptor Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175375",
        "className": "Peroxisome Proliferator-activated Receptor alpha Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175376",
        "className": "Peroxisome Proliferator-activated Receptor beta Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175377",
        "className": "Peroxisome Proliferator-activated Receptor gamma Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175378",
        "className": "Peroxisome Proliferator-activated Receptor delta Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175411",
        "className": "BRAF Serine/Threonine Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175436",
        "className": "Neuraminidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175441",
        "className": "Complement 5 Inactivators",
        "classType": "MOA"
      },
      {
        "classId": "N0000175444",
        "className": "Chemokine Co-receptor 5 Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175445",
        "className": "Chemokine Co-receptor 5 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175446",
        "className": "Tumor Necrosis Factor alpha Receptor Blocking Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175447",
        "className": "Tumor Necrosis Factor beta Receptor Blocking Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175448",
        "className": "Potassium Channel Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175449",
        "className": "Potassium Channel Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175450",
        "className": "Corticosteroid Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175451",
        "className": "Tumor Necrosis Factor Receptor Blocking Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175456",
        "className": "Chloride Channel Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000175458",
        "className": "Calcineurin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175472",
        "className": "Metal Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175513",
        "className": "Dipeptidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175516",
        "className": "RNA Synthetase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175532",
        "className": "Demulcent Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175533",
        "className": "Dyes",
        "classType": "MOA"
      },
      {
        "classId": "N0000175534",
        "className": "Irrigation",
        "classType": "MOA"
      },
      {
        "classId": "N0000175542",
        "className": "M2 Protein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175545",
        "className": "Oxidation-Reduction Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175546",
        "className": "Oxidation Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175547",
        "className": "Reduction Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175548",
        "className": "Disulfide Reduction",
        "classType": "MOA"
      },
      {
        "classId": "N0000175549",
        "className": "Cystine Disulfide Reduction",
        "classType": "MOA"
      },
      {
        "classId": "N0000175614",
        "className": "Fusion Protein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175615",
        "className": "Fusion Protein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175617",
        "className": "CD3-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175618",
        "className": "CD3 Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175619",
        "className": "CD3 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175621",
        "className": "Interleukin 2 Receptor-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175624",
        "className": "mTOR Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175635",
        "className": "Factor Xa Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175636",
        "className": "Antithrombin 3 Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000175652",
        "className": "Cholinergic Ganglionic Nicotinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175653",
        "className": "Cholinergic Ganglionic Nicotinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175654",
        "className": "Gonadotropin Releasing Hormone Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175684",
        "className": "Full Opioid Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175685",
        "className": "Partial Opioid Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175686",
        "className": "Competitive Opioid Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175687",
        "className": "Noncompetitive Opioid Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175701",
        "className": "Full Cholinergic Nicotinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175702",
        "className": "Partial Cholinergic Nicotinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175703",
        "className": "Competitive Cholinergic Nicotinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175704",
        "className": "Noncompetitive Cholinergic Nicotinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175707",
        "className": "Interleukin 1 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175708",
        "className": "Interleukin 1 Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175709",
        "className": "Interleukin 1 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175717",
        "className": "Cholinergic Neuromuscular Nicotinic Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175718",
        "className": "Cholinergic Neuromuscular Nicotinic Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175755",
        "className": "DOPA Decarboxylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175756",
        "className": "Catechol O-Methyltransferase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175760",
        "className": "Monoamine Oxidase-A Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175761",
        "className": "Monoamine Oxidase-B Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175763",
        "className": "Serotonin 1b Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175764",
        "className": "Serotonin 1d Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175770",
        "className": "Acetylcholine Release Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175773",
        "className": "Integrin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175774",
        "className": "Integrin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175787",
        "className": "Adenosine Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175788",
        "className": "Adenosine Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175789",
        "className": "Adenosine Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175792",
        "className": "IgE-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175799",
        "className": "Dopamine D2 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175804",
        "className": "Methylating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175806",
        "className": "Ammonium Ion Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175808",
        "className": "Hydroxyphenylpyruvate Dioxygenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175816",
        "className": "Serotonin 4 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175817",
        "className": "Serotonin 3 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175833",
        "className": "Acidifying Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175834",
        "className": "Magnesium Ion Exchange Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175844",
        "className": "Decarboxylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175850",
        "className": "Melanin Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175856",
        "className": "CD11a-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175857",
        "className": "CD2-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175862",
        "className": "Magnetic Resonance Contrast Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175868",
        "className": "gamma-Ray Emitting Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175869",
        "className": "Positron Emitting Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175890",
        "className": "Topoisomerase 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175899",
        "className": "Renin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175912",
        "className": "Dipeptidyl Peptidase 4 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175917",
        "className": "Thyroid Hormone Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175919",
        "className": "Cholesterol Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175920",
        "className": "Steroid Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175921",
        "className": "Adrenal Steroid Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175922",
        "className": "Sex Hormone Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175923",
        "className": "Glucocorticoid Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175924",
        "className": "Mineralocorticoid Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175925",
        "className": "Androgen Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175926",
        "className": "Estrogen Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175927",
        "className": "Progestin Synthesis Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175955",
        "className": "5-Lipoxygenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175962",
        "className": "Lead Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175964",
        "className": "Vitamin K Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175967",
        "className": "Thrombopoietin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000175968",
        "className": "Thrombopoietin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000175974",
        "className": "Complement Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000175981",
        "className": "Heparin Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000175985",
        "className": "Antibody-Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000177911",
        "className": "CD25-directed Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000178291",
        "className": "Vascular Endothelial Growth Factor-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000178332",
        "className": "Vesicular Monoamine Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000178333",
        "className": "Vesicular Monoamine Transporter Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000178377",
        "className": "Potassium Ion Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000180999",
        "className": "Angiotensin 2 Type 1 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181000",
        "className": "Fatty Acid Synthetase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000181001",
        "className": "Siderophore Iron Chelating Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000181002",
        "className": "HIV Fusion Protein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000181003",
        "className": "Histamine H3 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181004",
        "className": "Sodium Channel Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181005",
        "className": "Sodium Chloride Symporter Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000181006",
        "className": "Sodium Potassium Chloride Symporter Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000181007",
        "className": "Cannabinoid Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000181008",
        "className": "Cannabinoid Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181009",
        "className": "Cannabinoid Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181010",
        "className": "Ionophoric Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000181812",
        "className": "Lysophospholipid Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000181813",
        "className": "Lysosphingolipid Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000181814",
        "className": "Sphingosine 1-Phosphate Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000181815",
        "className": "Sphingosine 1-Phosphate Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000181817",
        "className": "Competitive NMDA Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181818",
        "className": "Noncompetitive NMDA Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181819",
        "className": "Uncompetitive NMDA Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000181820",
        "className": "NMDA Receptor Glycine Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182136",
        "className": "Cytochrome P450 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182137",
        "className": "Cytochrome P450 2D6 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182138",
        "className": "Cytochrome P450 1A2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182139",
        "className": "Cytochrome P450 2B6 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182140",
        "className": "Cytochrome P450 2C19 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182141",
        "className": "Cytochrome P450 3A4 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182143",
        "className": "P2Y12 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182145",
        "className": "Sigma Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182146",
        "className": "Sigma-1 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182147",
        "className": "Sigma-1 Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182148",
        "className": "Sigma-1 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182150",
        "className": "CD80-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182151",
        "className": "CD86-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182632",
        "className": "Cytochrome P450 17A1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182634",
        "className": "CTLA-4-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182636",
        "className": "B Lymphocyte Stimulator-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182638",
        "className": "HCV NS3/4A Protease Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182727",
        "className": "Sodium Channel Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182728",
        "className": "Potassium Channel Openers",
        "classType": "MOA"
      },
      {
        "classId": "N0000182960",
        "className": "Phosphodiesterase 4 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000182962",
        "className": "Bradykinin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000182963",
        "className": "Bradykinin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182964",
        "className": "Bradykinin B2 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000182966",
        "className": "CD30-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000184008",
        "className": "Antigen Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184009",
        "className": "Virus Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184010",
        "className": "Venom Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184011",
        "className": "Bacterial Toxin Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184012",
        "className": "Endogenous Antigen Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184141",
        "className": "Bacterial Neurotoxin Neutralization",
        "classType": "MOA"
      },
      {
        "classId": "N0000184142",
        "className": "Ligase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000184143",
        "className": "Carbamoyl Phosphate Synthetase 1 Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000184145",
        "className": "Chloride Channel Activation Potentiators",
        "classType": "MOA"
      },
      {
        "classId": "N0000184147",
        "className": "Smoothened Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000184148",
        "className": "Smoothened Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000184150",
        "className": "Digoxin Binding Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000184151",
        "className": "Kallikrein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000185007",
        "className": "Adrenergic beta3-Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000185009",
        "className": "Serotonin 2c Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000185498",
        "className": "Lyase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000185499",
        "className": "Guanylate Cyclase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000185501",
        "className": "Dihydroorotate Dehydrogenase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000185503",
        "className": "P-Glycoprotein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000185504",
        "className": "Cytochrome P450 2C9 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000185505",
        "className": "Cytochrome P450 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000185506",
        "className": "Cytochrome P450 3A4 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000185507",
        "className": "Cytochrome P450 2C9 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000185607",
        "className": "Cytochrome P450 2C19 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000186778",
        "className": "Microsomal Triglyceride Transfer Protein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187052",
        "className": "alpha-Particle Emitting Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000187054",
        "className": "RANK Ligand Blocking Activity",
        "classType": "MOA"
      },
      {
        "classId": "N0000187056",
        "className": "Sodium-Glucose Transporter Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187057",
        "className": "Sodium-Glucose Transporter 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187058",
        "className": "Sodium-Glucose Transporter 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187060",
        "className": "Organic Cation Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000187061",
        "className": "Organic Cation Transporter 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187062",
        "className": "Cytochrome P450 2C8 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000187063",
        "className": "Cytochrome P450 2C8 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000187064",
        "className": "Cytochrome P450 2B6 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000188367",
        "className": "Membrane Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188368",
        "className": "Fatty Acid Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188369",
        "className": "Microsomal Triglyceride Transfer Protein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188370",
        "className": "Ion Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188371",
        "className": "Anion Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188372",
        "className": "Cation Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188373",
        "className": "Proton Pump Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188374",
        "className": "Antiporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188375",
        "className": "Symporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188376",
        "className": "Sodium Chloride Symporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188377",
        "className": "Sodium Potassium Chloride Symporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188378",
        "className": "Sodium-Glucose Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188379",
        "className": "Monosaccharide Transporter Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188380",
        "className": "Ligand-Gated Ion Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000188381",
        "className": "Ionotropic Glutamate Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190107",
        "className": "Organic Anion Transporting Polypeptide 1B1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190108",
        "className": "Organic Anion Transporting Polypeptide 1B3 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190109",
        "className": "Organic Anion Transporting Polypeptide 2B1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190110",
        "className": "Organic Anion Transporter 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190111",
        "className": "Organic Anion Transporter 3 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190112",
        "className": "Breast Cancer Resistance Protein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190113",
        "className": "Breast Cancer Resistance Protein Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190114",
        "className": "Cytochrome P450 3A Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190115",
        "className": "Cytochrome P450 3A5 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190116",
        "className": "Enzyme Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000190117",
        "className": "UDP Glucuronosyltransferases Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000190118",
        "className": "Cytochrome P450 3A Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000190477",
        "className": "Interleukin 6 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190478",
        "className": "Interleukin 6 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190479",
        "className": "Interleukin 6 Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190481",
        "className": "Hydroxylase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000190482",
        "className": "Phenylalanine Hydroxylase Activators",
        "classType": "MOA"
      },
      {
        "classId": "N0000190484",
        "className": "Guanylate Cyclase Stimulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000190855",
        "className": "Vesicular Monoamine Transporter 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190857",
        "className": "Janus Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000190989",
        "className": "Glucagon-like Peptide-2 (GLP-2) Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190990",
        "className": "Glucagon-like Peptide-2 (GLP-2) Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190991",
        "className": "Glucagon-like Peptide-2 (GLP-2) Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190993",
        "className": "Protease-activated Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190994",
        "className": "Protease-activated Receptor-1 Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190995",
        "className": "Protease-activated Receptor-1 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190997",
        "className": "Orexin Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000190998",
        "className": "Orexin Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000190999",
        "className": "Orexin Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191002",
        "className": "Antibody-Soluble Protein Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191003",
        "className": "Interleukin-6-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191004",
        "className": "Interleukin-6 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191005",
        "className": "Vascular Endothelial Growth Factor Receptor 2-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191258",
        "className": "RNA Replicase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191259",
        "className": "Programmed Death Receptor-1-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191262",
        "className": "Selective Progesterone Receptor Modulators",
        "classType": "MOA"
      },
      {
        "classId": "N0000191264",
        "className": "P-Glycoprotein Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000191265",
        "className": "Organic Cation Transporter 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191266",
        "className": "Cytochrome P450 1A2 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000191267",
        "className": "Cytochrome P450 2D6 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000191268",
        "className": "Cytochrome P450 3A5 Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000191269",
        "className": "UDP Glucuronosyltransferases Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191270",
        "className": "UGT1A Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191271",
        "className": "UGT2B Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191272",
        "className": "UGT1A1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191273",
        "className": "UGT2B7 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191274",
        "className": "UGT2B15 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191275",
        "className": "UGT1A3 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191276",
        "className": "UGT1A4 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191277",
        "className": "UGT1A6 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191278",
        "className": "UGT1A9 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191422",
        "className": "CD19-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191423",
        "className": "Multidrug and Toxin Extrusion Transporter 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191494",
        "className": "Interleukin-17A Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191496",
        "className": "Interleukin-17A-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191498",
        "className": "Antibody-Surface Glycolipid Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191499",
        "className": "Glycolipid Disialoganglioside-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191501",
        "className": "Antibody-Soluble Antigen Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191502",
        "className": "Antibody-Surface Antigen Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191545",
        "className": "Hyperpolarization-activated Cyclic Nucleotide-gated Channel Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000191546",
        "className": "Hyperpolarization-activated Cyclic Nucleotide-gated Channel Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191547",
        "className": "Hyperpolarization-activated Cyclic Nucleotide-gated Channel Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191622",
        "className": "Poly(ADP-Ribose) Polymerase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191624",
        "className": "Cytochrome P450 1A Inducers",
        "classType": "MOA"
      },
      {
        "classId": "N0000191725",
        "className": "Metalloprotease Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191726",
        "className": "Metalloendopeptidase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191727",
        "className": "Neprilysin Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191729",
        "className": "Proprotein Convertase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191730",
        "className": "PCSK9 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191866",
        "className": "Opioid mu-Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000191868",
        "className": "Pyrimidine Phosphorylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191869",
        "className": "Thymidine Phosphorylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000191871",
        "className": "Interleukin-5 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000192335",
        "className": "CD38-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000192337",
        "className": "SLAMF7-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000192340",
        "className": "Urate Transporter 1 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000192345",
        "className": "Epoxide Hydrolase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000192560",
        "className": "Farnesoid X Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000192700",
        "className": "Lymphocyte Function-Associated Antigen-1 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000192702",
        "className": "Anthrax Protective Antigen-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000192796",
        "className": "Interleukin-12 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000192798",
        "className": "Interleukin-23 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193178",
        "className": "Vasopressin V2 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193179",
        "className": "Vasopressin V1 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193180",
        "className": "Vasopressin V3 Receptor Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193277",
        "className": "Interleukin 17 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193278",
        "className": "Interleukin 17 Receptor A Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193279",
        "className": "Interleukin 17 Receptor A Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193302",
        "className": "Tryptophan Hydroxylase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000193336",
        "className": "Programmed Death Ligand-1-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193337",
        "className": "Programmed Death Ligand-1 Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193339",
        "className": "Interleukin 4 Receptor alpha-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193340",
        "className": "Interleukin 4 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193341",
        "className": "Interleukin 4 Receptor alpha Antagonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193542",
        "className": "Vascular Endothelial Growth Factor Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000193544",
        "className": "CD19 Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193614",
        "className": "Isocitrate Dehydrogenase 2 Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000193616",
        "className": "CD22-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193786",
        "className": "Growth Hormone Secretagogue Receptor Agonists",
        "classType": "MOA"
      },
      {
        "classId": "N0000193787",
        "className": "Growth Hormone Secretagogue Receptor Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193789",
        "className": "Rho Kinase Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000193791",
        "className": "Gene Alterations",
        "classType": "MOA"
      },
      {
        "classId": "N0000193792",
        "className": "Gene Editing or Modification",
        "classType": "MOA"
      },
      {
        "classId": "N0000193793",
        "className": "Gene Expression Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193794",
        "className": "Gene Transcription Modulation",
        "classType": "MOA"
      },
      {
        "classId": "N0000193795",
        "className": "Gene Transduction or Replacement",
        "classType": "MOA"
      },
      {
        "classId": "N0000193796",
        "className": "Gene Translation Modulation",
        "classType": "MOA"
      },
      {
        "classId": "N0000193801",
        "className": "DNA Terminase Complex Inhibitors",
        "classType": "MOA"
      },
      {
        "classId": "N0000193802",
        "className": "Interleukin 5 Receptor alpha-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193805",
        "className": "CD4-directed Antibody Interactions",
        "classType": "MOA"
      },
      {
        "classId": "N0000193807",
        "className": "HIV 1 Post-attachment Fusion Inhibitors",
        "classType": "MOA"
      }
    ]
  }
}
```
