# example: classTree #

## usage: classTree ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    const classId = 'N0000185505' // Cytochrome P450 Inducers
    new RxnavApi().json().rxclass().classTree(classId)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

```

### output ###
```
{
  "userInput": {
    "classId": "N0000185505"
  },
  "rxclassTree": [
    {
      "rxclassMinConceptItem": {
        "classId": "N0000185505",
        "className": "Cytochrome P450 Inducers",
        "classType": "MOA"
      },
      "rxclassTree": [
        {
          "rxclassMinConceptItem": {
            "classId": "N0000187064",
            "className": "Cytochrome P450 2B6 Inducers",
            "classType": "MOA"
          }
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000185507",
            "className": "Cytochrome P450 2C9 Inducers",
            "classType": "MOA"
          }
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000191624",
            "className": "Cytochrome P450 1A Inducers",
            "classType": "MOA"
          },
          "rxclassTree": [
            {
              "rxclassMinConceptItem": {
                "classId": "N0000191266",
                "className": "Cytochrome P450 1A2 Inducers",
                "classType": "MOA"
              }
            }
          ]
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000185607",
            "className": "Cytochrome P450 2C19 Inducers",
            "classType": "MOA"
          }
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000191267",
            "className": "Cytochrome P450 2D6 Inducers",
            "classType": "MOA"
          }
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000190118",
            "className": "Cytochrome P450 3A Inducers",
            "classType": "MOA"
          },
          "rxclassTree": [
            {
              "rxclassMinConceptItem": {
                "classId": "N0000191268",
                "className": "Cytochrome P450 3A5 Inducers",
                "classType": "MOA"
              }
            },
            {
              "rxclassMinConceptItem": {
                "classId": "N0000185506",
                "className": "Cytochrome P450 3A4 Inducers",
                "classType": "MOA"
              }
            }
          ]
        },
        {
          "rxclassMinConceptItem": {
            "classId": "N0000187063",
            "className": "Cytochrome P450 2C8 Inducers",
            "classType": "MOA"
          }
        }
      ]
    }
  ]
}
```