# example: spellingSuggestions #

## usage: spellingSuggestions ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let term = 'marcolides'
    let type = 'CLASS' // 'DRUG' or 'CLASS'
    new RxnavApi().json().rxclass().spellingSuggestions(term, type)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

```

### output ###
```
{
  "userInput": {
    "type": "CLASS",
    "term": "marcolides"
  },
  "suggestionList": {
    "suggestion": [
      "macrolides",
      "macrolide",
      "mannosides",
      "ketolides",
      "cardenolides",
      "maleimides",
      "morpholines",
      "cardanolides"
    ]
  }
}
```