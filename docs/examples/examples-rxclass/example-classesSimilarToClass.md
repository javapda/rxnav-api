# example: classesSimilarToClass #

## usage: classesSimilarToClass ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let classId = 'L01XE' // protein kinase inhibitors
    let sourceOfDrugRelations = 'ATC'
    let relationship = null //'has_EPC'
    let scoreType = 1  // optional; 0=default, 1=includes, 2=included_in
    let numberOfResultsToReturn = 2
    let equivalenceThreshold = null // 0.0-1.0
    let inclusionThreshold = null // 0.0-1.0 [only relevant for scoreType=1-or-2]

    const payload = {
        classId,
        relaSource: sourceOfDrugRelations,
        rela: relationship,
        scoreType,
        top: numberOfResultsToReturn,
        equivalenceThreshold,
        inclusionThreshold
    }
    new RxnavApi().json().rxclass().classesSimilarToClass(payload)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
```

### output ###
```
{
  "userInput": {
    "classId": "L01XE",
    "relaSource": "ATC",
    "rela": "",
    "scoreType": "1",
    "top": "2"
  },
  "similarityMember": {
    "ingredientRxcui": [
      "1242999",
      "1592737",
      "1307619",
      "475342",
      "1873916",
      "1147220",
      "1148495",
      "1312397",
      "1425099",
      "1727455",
      "1722365",
      "662281",
      "1603296",
      "1193326",
      "657797",
      "328134",
      "1601374",
      "2049122",
      "495881",
      "714438",
      "1363268",
      "141704",
      "480167",
      "1721560",
      "1430438",
      "337525",
      "357977",
      "1098413",
      "1442981",
      "1424911",
      "1535457",
      "282388",
      "1919083",
      "1921217",
      "1364347"
    ],
    "rankClassConcept": [
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000190111",
            "className": "Organic Anion Transporter 3Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000191265",
            "className": "Organic Cation Transporter 1 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "fdaspl",
          "rxclassMinConceptItem": {
            "classId": "N0000190110",
            "className": "Organic Anion Transporter 1 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000190110",
            "className": "Organic Anion Transporter 1 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "fdaspl",
          "rxclassMinConceptItem": {
            "classId": "N0000190111",
            "className": "Organic Anion Transporter 3 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "fdaspl",
          "rxclassMinConceptItem": {
            "classId": "N0000191265",
            "className": "Organic Cation Transporter 1 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "fdaspl",
          "rxclassMinConceptItem": {
            "classId": "N0000182139",
            "className": "Cytochrome P450 2B6 Inhibitors",
            "classType": "MOA"
          }
        }
      },
      {
        "similarityEntityItem": {
          "equivalenceScore": "0.0127775315",
          "inclusionScore": "1.0",
          "intersection": "1",
          "cardinality1": "35",
          "cardinality2": "1"
        },
        "drugClassConceptItem": {
          "rela": "has_moa",
          "relaSource": "dailymed",
          "rxclassMinConceptItem": {
            "classId": "N0000182139",
            "className": "Cytochrome P450 2B6 Inhibitors",
            "classType": "MOA"
          }
        }
      }
    ]
  }
}
```