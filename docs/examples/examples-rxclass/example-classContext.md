# example: classContext #

## usage: classContext ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
let classId = 'D019275'  // Radiopharmaceuticals

new RxnavApi()
.rxclass()
.classContext(classId)
.then((results)=>{
    console.log(`${JSON.stringify(results)}`)
})
.catch((err)=>console.log(`err=${err}`))

```

### output ###
```
{
  "userInput": {
    "classId": "D019275"
  },
  "classPathList": {
    "classPath": [
      {
        "rxclassMinConcept": [
          {
            "classId": "D019275",
            "className": "Radiopharmaceuticals",
            "classType": "MESHPA"
          },
          {
            "classId": "D064907",
            "className": "Diagnostic Uses of Chemicals",
            "classType": "MESHPA"
          },
          {
            "classId": "D020228",
            "className": "MeSH Pharmacologic Actions (MESHPA)",
            "classType": "MESHPA"
          }
        ]
      },
      {
        "rxclassMinConcept": [
          {
            "classId": "D019275",
            "className": "Radiopharmaceuticals",
            "classType": "MESHPA"
          },
          {
            "classId": "D045504",
            "className": "MolecularMechanisms of Pharmacological Action",
            "classType": "MESHPA"
          },
          {
            "classId": "D020228",
            "className": "MeSH Pharmacologic Actions (MESHPA)",
            "classType": "MESHPA"
          }
        ]
      },
      {
        "rxclassMinConcept": [
          {
            "classId": "D019275",
            "className": "Radiopharmaceuticals",
            "classType": "CHEM"
          },
          {
            "classId": "D007202",
            "className": "Indicators and Reagents",
            "classType": "CHEM"
          },
          {
            "classId": "D019995",
            "className": "Laboratory Chemicals",
            "classType": "CHEM"
          },
          {
            "classId": "D020313",
            "className": "Specialty Uses of Chemicals",
            "classType": "CHEM"
          },
          {
            "classId": "D020164",
            "className": "Chemical Actions and Uses",
            "classType": "CHEM"
          },
          {
            "classId": "Z2",
            "className": "Substances",
            "classType": "CHEM"
          },
          {
            "classId": "Z1",
            "className": "Substances and Cells (CHEM)",
            "classType": "CHEM"
          }
        ]
      }
    ]
  }
}
```