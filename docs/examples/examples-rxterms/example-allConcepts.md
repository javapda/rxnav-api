# example: version #

## usage: version ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxterms().allConcepts()
        .then((res) => {
            console.log(JSON.stringify(res))
            console.log(`Number found: ${res.minConceptGroup.minConcept.length}`)
        })
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
...lots of output
Number found: 20964
```