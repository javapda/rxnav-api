# example: rxnormDisplayName #

## usage: rxnormDisplayName ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let rxnorm = '198440'
    new RxnavApi().rxterms().rxnormDisplayName(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{
  "displayGroup": {
    "rxcui": "198440",
    "displayName": "Acetaminophen (Oral Pill)"
  }
}
```