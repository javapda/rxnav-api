# example: rxnormTerms #

## usage: rxnormTerms ##

### node ###
```
const {RxnavApi} = require('rxnav-api')
    let rxnorm = '198440' // Acetaminophen
    new RxnavApi().rxterms().rxnormTerms(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{
  "rxtermsProperties": {
    "brandName": "",
    "displayName": "Acetaminophen (Oral Pill)",
    "synonym": "APAP",
    "fullName": "Acetaminophen 500 MG Oral Tablet",
    "fullGenericName": "Acetaminophen 500 MG Oral Tablet",
    "strength": "500 mg",
    "rxtermsDoseForm": "Tab",
    "route": "Oral Pill",
    "termType": "SCD",
    "rxcui": "198440",
    "genericRxcui": "0",
    "rxnormDoseForm": "Oral Tablet",
    "suppress": ""
  }
}
```