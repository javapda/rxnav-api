# example: version #

## usage: version ##

### node ###
```
const {RxnavApi} = require('rxnav-api')

    new RxnavApi().rxterms().version()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
        
```
### output ###
```
{"rxtermsVersion":"RxTerms201811"}
```