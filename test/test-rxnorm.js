/**
 * test-rxnorm.js
 * 
 * shows examples
 * 
 */
const RxnavApi = require('../src/rxnav-api')
const { UmlsApi } = require('umls-api')
const fs = require("fs")
const moment = require('moment')
const os = require('os')

const allConceptsByTermTypeList = () => {
    let types = ['BPCK']
    types = ['GPCK']
    // types = ['BN']
    const rxnavApi = new RxnavApi()
    rxnavApi.json().rxnorm().allConceptsByTermTypeList(types)//['BN','BPCK'])
        .then((res) => {
            if (rxnavApi.isJson()) {
                console.log(`${JSON.stringify(res)}`)
                // console.log(`No. ${types} : ${res.minConceptGroup.minConcept.length}`)
            } else {
                console.log(`xml (not json) so dumping all xml\n\n${res}`)
            }
        }
        )
        .catch(err => console.log(`err=${err}`))

}
// allConceptsByTermTypeList()

const approximateTerm = () => {
    new RxnavApi().xml().rxnorm().approximateTerm({ term: 'zocor 10 mg', option: 1, maxEntries: 3 })
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// approximateTerm()
const brandsContainingIngredients = () => {
    new RxnavApi().xml().rxnorm().brandsContainingIngredients([8896, 20610])
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// brandsContainingIngredients()

const displayNames = () => {
    new RxnavApi().json().rxnorm().displayNames()
        .then((res) =>
            console.log(res))
        .catch(err => console.log(`err=${err}`))
}
// displayNames()

const drugs = () => {
    let searchTerm = 'cymbalta'
    searchTerm = 'warfarin'
    searchTerm = 'Warfarin Sodium 1 MG'
    //searchTerm = 'cannabis'
    new RxnavApi().json().rxnorm().drugs(searchTerm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
//drugs()
const idTypeList = () => {
    new RxnavApi().json().rxnorm().idTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// idTypeList()

const ndcProperties = () => {
    let ndc = '1C5BC1DD-E9EC-44C1-9281-67AD482315D9'
    ndc = '213270'
    const rxnorm = new RxnavApi().json().rxnorm()
    rxnorm.ndcProperties(ndc)
        .then((ndcinfo) =>
            console.log(JSON.stringify(ndcinfo)))
        .catch(err => console.log(`err=${err}`))
}
// ndcProperties()
const ndcStatus = () => {
    let ndc = '1C5BC1DD-E9EC-44C1-9281-67AD482315D9'
    ndc = '213270'
    ndc = '00143314501'
    let payload = {
        ndc: ndc,
        start: null,
        end: null,
        history: 0,
        altpkg: 0
    }
    new RxnavApi().json().rxnorm().ndcStatus(payload)
        .then((ndcStatusInfo) =>
            console.log(JSON.stringify(ndcStatusInfo)))
        .catch(err => console.log(`err=${err}`))
}
// ndcStatus()

const propertyCategoryList = () => {
    new RxnavApi().json().rxnorm().propertyCategoryList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// propertyCategoryList()

const propertyNameList = () => {
    new RxnavApi().json().rxnorm().propertyNameList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// propertyNameList()

const rxnormRelationTypeList = () => {
    new RxnavApi().json().rxnorm().relationTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxnormRelationTypeList()

const spellingSuggestionList = () => {
    new RxnavApi().json().rxnorm().spellingSuggestionList('ambien')
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// spellingSuggestionList()


const termTypes = () => {
    new RxnavApi().json().rxnorm().termTypes()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// termTypes()

const rxNormVersion = () => {
    new RxnavApi().json().rxnorm().version()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxNormVersion()


const ndcsForRxnorm = () => {
    new RxnavApi().json().rxnorm().rxcui().ndcsForRxnorm(1668240)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// ndcsForRxnorm()

/**
 * In the example below, the TTY property for morphine (RxCUI=7052) 
 * is checked to see if the concept is an ingredient.
 */
const doesRxnormHaveProperty = () => {
    let morphineRxnorm = 7052
    new RxnavApi().json().rxnorm().doesRxnormHaveProperty(morphineRxnorm, { propName: 'TTY', propValues: ['IN'] })
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// doesRxnormHaveProperty()

const rxnormsForOtherVocabulary = () => {
    let termType = 'GCN_SEQNO'
    const gcnSeqno = '009172'
    let otherVocabularyCode = gcnSeqno
    termType = 'NDC'
    otherVocabularyCode = '0781-1506-10'
    // termType = 'NUI'
    // otherVocabularyCode = 'N0000189366'
    new RxnavApi().xml().rxnorm().rxnormsForOtherVocabulary(termType, otherVocabularyCode)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxnormsForOtherVocabulary()

const rxnormsForFdbGcnSeqno = () => {
    let gcnSeqno = '009172'
    new RxnavApi().rxnorm().rxnormsForFdbGcnSeqno(gcnSeqno)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormsForFdbGcnSeqno()

const rxnormsForHcpcsCode = () => {
    let hcpcsCode = 'J2710'
    new RxnavApi().rxnorm().rxnormsForHcpcsCode(hcpcsCode)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormsForHcpcsCode()


const rxnormsForSnomedCode = () => {
    let snomedCode = '372767007'// '1039008'
    new RxnavApi().rxnorm().rxnormsForSnomedCode(snomedCode)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormsForSnomedCode()

const rxnormsForNdc = () => {
    let ndc = '11523-7020-1'    //'00904629161'
    new RxnavApi().rxnorm().rxnormsForNdc(ndc)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormsForNdc()

const rxnormProperties = () => {
    let rxnorm = '131725' // ambien
    new RxnavApi().json().rxnorm().rxnormProperties(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormProperties()

const rxnormProperty = () => {
    let rxnorm = '7052' // morphine
    let property = 'ATC'
    new RxnavApi().json().rxnorm().rxnormProperty(rxnorm, property)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormProperty()

const rxnormNdcList = () => {
    let rxnorm = '213269' // Viagra 25 MG Oral Tablet 
    new RxnavApi().xml().rxnorm().rxnormNdcList(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormNdcList()

const rxnormsForFdbIngredientId = () => {
    let fdbIngredientId = '004489'
    new RxnavApi().rxnorm().rxnormsForFdbIngredientId(fdbIngredientId)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormsForFdbIngredientId()

const rxnormStatus = () => {
    let rxnorm = '105048'
    rxnorm = '44'
    // rxnorm = '1805014'
    new RxnavApi().json().rxnorm().rxnormStatus(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormStatus()

const rxnormRelatedConceptList = () => {
    let rxnorm = '213269' // Viagra 25 MG Oral Tablet 
    new RxnavApi().json().rxnorm().rxnormRelatedConceptList(rxnorm)
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormRelatedConceptList()


const rxnormRelatedConceptListByTermTypes = () => {
    let rxnorm = '174742' // Plavix
    // SBD+SBDF
    new RxnavApi().json().rxnorm().rxnormRelatedConceptListByTermTypes(rxnorm, ['SBD', 'SBDF'])
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormRelatedConceptListByTermTypes()

const rxnormRelatedConceptListByRelationalAttributes = () => {
    let rxnorm = '174742' // Plavix
    // tradename_of+has_precise_ingredient
    new RxnavApi().json().rxnorm().rxnormRelatedConceptListByRelationalAttributes(rxnorm, ['tradename_of', 'has_precise_ingredient'])
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxnormRelatedConceptListByRelationalAttributes()

const loadCredentials = () => {
    //console.log(`hostname:${os.hostname()} : ${JSON.stringify(os.userInfo())}`)

    const homedir = os.homedir();
    const umlsFilePath = `${homedir}/.umls`
    if (fs.existsSync(umlsFilePath)) {
        const content = fs.readFileSync(umlsFilePath)
        const umlsCredentials = JSON.parse(content);
        umlsCredentials.createMoment = moment()
        return umlsCredentials
    } else {
        throw `unable to find file ${umlsFilePath}`
    }
}

const rxnormProprietaryInfo = () => {
    const umlsApi = new UmlsApi(loadCredentials())
    umlsApi.authenticate()
        .then((authenticatedUmlsApi) => {
            authenticatedUmlsApi.serviceTicket()
                .then((svcTicket) => {
                    let rxnorm = '261455' // Viagra 25 MG Oral Tablet 
                    rxnorm = '178'  // FDB Acetone
                    rxnorm = '2670' // FDB codeine
                    rxnorm = '197844' // ISOtretinoin_Claravis_20mg_cap
                    rxnorm = '197531'
                    new RxnavApi().json().rxnorm().rxnormProprietaryInfo(rxnorm, ['MSH', 'RXNORM'], svcTicket)
                        .then((res) =>
                            console.log(JSON.stringify(res)))
                        .catch(err => console.log(`err=${err}`))

                })
                .catch((err) => { console.log(`DDDD, err=${err}`) })
        })
        .catch((err) => console.log(`OUCH,err=${err}`))
}
// rxnormProprietaryInfo()

