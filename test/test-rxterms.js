/**
 * test-rxterms.js
 * 
 * shows examples
 * 
 */
const RxnavApi = require('../src/rxnav-api')


const rxtermsVersion = () => {
    new RxnavApi().json().rxterms().version()
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// rxtermsVersion()

const rxtermsAllConcepts = () => {
    new RxnavApi().json().rxterms().allConcepts()
        .then((res) => {
            console.log(JSON.stringify(res))
            console.log(`Number found: ${res.minConceptGroup.minConcept.length}`)
        })
        .catch(err => console.log(`err=${err}`))

}
// rxtermsAllConcepts()

const rxtermsRxnormDisplayName = () => {
    let rxnorm = '198440' // Acetaminophen
    new RxnavApi().json().rxterms().rxnormDisplayName(rxnorm)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxtermsRxnormDisplayName()

const rxtermsRxnormTerms = () => {
    let rxnorm = '198440' // Acetaminophen
    new RxnavApi().json().rxterms().rxnormTerms(rxnorm)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxtermsRxnormTerms()
