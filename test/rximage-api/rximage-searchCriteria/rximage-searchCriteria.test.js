const RximageSearchCriteria = require('../../../src/rximage-api/rximage-searchCriteria')

describe('testing search criteria',() => {
    let searchCriteria;
    beforeAll(()=>{
    })
    beforeEach(()=>{
        searchCriteria=new RximageSearchCriteria()
        expect(searchCriteria).not.toBeNull()
    })
    test('constructor',()=>{
        expect(searchCriteria.params()).not.toBeNull()
        expect(searchCriteria.size(3).params().size).toBe(3)
        expect(searchCriteria.color('blue').params().color).toBe('blue')
        expect(searchCriteria.id('fakeId').params().id).toBe('fakeId')
        expect(searchCriteria.imprint('imprint').params().imprint).toBe('imprint')
        expect(searchCriteria.imprintColor('imprintColor').params().imprintColor).toBe('imprintColor')
        expect(searchCriteria.imprintType('imprintType').params().imprintType).toBe('imprintType')
        expect(searchCriteria.inactive(true).params().inactive).toBeTruthy()
        expect(searchCriteria.inactive(false).params().inactive).toBeFalsy()
        expect(searchCriteria.matchPackSize(true).params().matchPackSize).toBeTruthy()
        expect(searchCriteria.matchPackSize(false).params().matchPackSize).toBeFalsy()
        expect(searchCriteria.matchRelabeled(true).params().matchRelabeled).toBeTruthy()
        expect(searchCriteria.matchRelabeled(false).params().matchRelabeled).toBeFalsy()
        expect(searchCriteria.name('somemedname').params().name).toBe('somemedname')
        expect(searchCriteria.ndc('somemedndc').params().ndc).toBe('somemedndc')
        expect(searchCriteria.parse('blue+round').params().parse).toBe('blue+round')
        expect(searchCriteria.rootId('somerootId').params().rootId).toBe('somerootId')
        expect(searchCriteria.rxnorm('somerxnorm').params().rxcui).toBe('somerxnorm')
        expect(searchCriteria.score(2).params().score).toBe(2)
        expect(searchCriteria.setId('89dd7e24-85fc-4152-89ea-47ec2b48a1ed').params().setId).toBe('89dd7e24-85fc-4152-89ea-47ec2b48a1ed')
        expect(searchCriteria.shape('TRIANGLE').params().shape).toBe('TRIANGLE')
        expect(searchCriteria.sizeTolerance(2).params().sizeT).toBe(2)
        expect(searchCriteria.symbol(true).params().symbol).toBeTruthy()
        expect(searchCriteria.symbol(false).params().symbol).toBeFalsy()

        expect(searchCriteria.includeActive(true).params().includeActive).toBeTruthy()
        expect(searchCriteria.includeActive(false).params().includeActive).toBeFalsy()

        expect(searchCriteria.includeInactive(true).params().includeInactive).toBeTruthy()
        expect(searchCriteria.includeInactive(false).params().includeInactive).toBeFalsy()

        expect(searchCriteria.includeIngredients(true).params().includeIngredients).toBeTruthy()
        expect(searchCriteria.includeIngredients(false).params().includeIngredients).toBeFalsy()

        expect(searchCriteria.includeMpc(true).params().includeMpc).toBeTruthy()
        expect(searchCriteria.includeMpc(false).params().includeMpc).toBeFalsy()

        expect(searchCriteria.limitNumberOfRecords(2).params().rLimit).toBe(2)
        
        expect(searchCriteria.pageNumber(2).params().rPage).toBe(2)
        expect(searchCriteria.pageSize(3).params().rPageSize).toBe(3)
        
        console.log(searchCriteria)
    })
})