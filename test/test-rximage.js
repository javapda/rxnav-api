/**
 * test-rximage.js
 * 
 * shows examples
 * 
 */
const RxnavApi = require('../src/rxnav-api')

const rximageSearch = () => {
    let rxnorm = '197698'
    let searchCriteria = {
        // image selection
        // size: 17,  // size in millimeters
        // sizeT : 0,  // size tolerance when searching by size
        // id : 185644464,
        // name : 'sertraline 100'
        // color:'blue',
        // imprint : 'teva,54',  // can have multiple things on it 
        // imprintColor : 'red',
        // inactive : 'gelatin',
        //ndc : '12345678901p3',
        // ndc : '11042-0142-03',
        //parse : 'blue+round' //+Limit=1',
        //shape : 'TRAPEZOID', //'DIAMOND', //'TRIANGLE',
        //matchRelabeled: false,  // used with ndc
        // matchPackSize : true,  // used with ndc  
        //rxnorm: '197698'  // not sure if this works - no results
        //score : 1, // number of sections the score breaks into
        symbol: false, // include or exclude pills with symbols or markings

        // results control
        rLimit: 1
    }
    new RxnavApi().json().rximage().search(searchCriteria, 'rxbase')
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rximageSearch()

const rximageSearchBySearchCriteria = () => {
    let searchCriteriaParams = new RxnavApi().rximage().createSearchCriteria()
        .color('blue+green')
        .color('brown')
        .limitNumberOfRecords(1)
        .params()
    new RxnavApi().json().rximage().search(searchCriteriaParams, 'rxnav')
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rximageSearchBySearchCriteria()

const rximageSearcher = () => {
    const rximageApi = new RxnavApi().rximage()
    const searchParams = rximageApi
        .createSearchCriteria()
        .name("atorvastatin")
        .limitNumberOfRecords(1)
        .resolution(300)
        .params()

    rximageApi
        .search(searchParams, 'rxnav')
        .then((results) => {
            console.log(`${JSON.stringify(results)}`)
        })
        .catch((err) => console.log(`err=${err}`))
}
// rximageSearcher()