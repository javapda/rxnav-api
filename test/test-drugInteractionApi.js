const RxnavApi = require('../src/rxnav-api')

const drugInterationApiVersionTest = () => {
    const rxnavApi = new RxnavApi().json()
    const drugInteractionApi = rxnavApi.drugInteractionApi()
    drugInteractionApi.version()
        .then((res) => {
            if (rxnavApi.isJson()) {
                console.log(JSON.stringify(res))
                // console.log(res.sourceVersionList)
                // console.log(res.sourceVersionList.sourceVersion[0])
                // console.log(res.sourceVersionList.sourceVersion[1])
            } else {
                console.log(res)
            }
        })
        .catch(err => console.log(`err=${err}`))
}
// drugInterationApiVersionTest()

const drugInterationApiSourcesTest = () => {
    const rxnavApi = new RxnavApi().json()
    const drugInteractionApi = rxnavApi.drugInteractionApi()
    drugInteractionApi.sources()
        .then((res) => {
            if (rxnavApi.isJson()) {
                console.log(JSON.stringify(res))
            } else {
                console.log(res)
            }
        }
        )
        .catch(err => console.log(`err=${err}`))
}
// drugInterationApiSourcesTest()



const drugInterationApiInteractionTest = () => {
    const rxnavApi = new RxnavApi().json()
    const drugInteractionApi = rxnavApi.drugInteractionApi()
    const rxnorm = "88014"
    // const sources = ["ONCHigh"]
    const sources = ["DrugBank"]
    drugInteractionApi.interaction(rxnorm, sources)
        .then((res) => {
            if (rxnavApi.isJson()) {
                console.log(JSON.stringify(res))
            } else {
                console.log(res)
            }
        }
        )
        .catch(err => console.log(`err=${err}`))
}
// drugInterationApiInteractionTest()

const drugInterationApiInteractionListTest = () => {
    const rxnavApi = new RxnavApi().json()
    const drugInteractionApi = rxnavApi.drugInteractionApi()
    const rxnorms = ["207106", "152923", "656659"]
    // const sources = ["ONCHigh"]
    const sources = ["DrugBank"]
    drugInteractionApi.interactionsBetweenDrugs(rxnorms, sources)
        .then((res) => {
            if (rxnavApi.isJson()) {
                console.log(JSON.stringify(res))
            } else {
                console.log(res)
            }
        }
        )
        .catch(err => console.log(`err=${err}`))
}
// drugInterationApiInteractionListTest()

