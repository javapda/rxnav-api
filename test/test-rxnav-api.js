/**
 * test-rxnav-api.js
 * 
 * shows examples
 * 
 */
const RxnavApi = require('../src/rxnav-api')

const restfulEndpointsList = () => {
    new RxnavApi().restfulEndpointsList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// restfulEndpointsList()

const rxnormSourceTypeList = () => {
    new RxnavApi().sourceTypeList()
        .then((res) =>
            console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// rxnormSourceTypeList()
