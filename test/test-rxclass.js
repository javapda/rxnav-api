/**
 * test-rxclass.js
 * 
 * shows examples
*/
const RxnavApi = require('../src/rxnav-api')

const drugClassesById = () => {
    let classId = 'B01AA' // ATC id	ATC1-4
    new RxnavApi().json().rxclass().byId(classId)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
//drugClassesById()

const drugClassesByName = () => {
    let className = 'radiopharmaceuticals'
    let classTypes = ["MESHPA"]
    // classTypes = null
    new RxnavApi().json().rxclass().byName(className, classTypes)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassesByName()

const classTypeList = () => {
    new RxnavApi().json().rxclass().classTypeList()
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
classTypeList()

const drugClassSourceTypeList = () => {
    new RxnavApi().json().rxclass().sourceTypeList()
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassSourceTypeList()

const drugClassRelationTypeList = () => {
    let relaSources = ['dailymed']
    new RxnavApi().xml().rxclass().relationTypeList(relaSources)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassRelationTypeList()

const drugClassClassesByRxnorm = () => {
    let rxnorm = '174742' // Plavix
    rxnorm = '7052' // morphine
    let optionalSourceList = ['MEDRT']
    let optionalRelationshipList = [] // ['may_treat']
    new RxnavApi().json().rxclass().classesByRxnorm(rxnorm, optionalSourceList, optionalRelationshipList)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassClassesByRxnorm()


const drugClassesByDrugName = () => {
    let drugName = 'morphine'
    let optionalRelationshipList = ['may_treat']
    let optionalSourceList = ['MEDRT']
    new RxnavApi().json().rxclass().byDrugName(drugName, optionalSourceList, optionalRelationshipList)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesByDrugName()

const drugClassesByClassTypeList = () => {
    let classTypeList = ['MOA']
    new RxnavApi().json().rxclass().byClassTypeList(classTypeList)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesByClassTypeList()


const drugClassesClassContext = () => {
    let classId = 'D019275'
    new RxnavApi().json().rxclass().classContext(classId)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesClassContext()


const drugClassesClassGraph = () => {
    let classId = 'D003879'
    let source = 'MESHPA'
    new RxnavApi().json().rxclass().classGraph(classId, source)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesClassContext()

const drugClassesClassMembers = () => {
    let classId = 'N0000008638' // Decreased GnRH Secretion
    let source = 'DAILYMED'
    let relationship = 'has_PE'
    let optionalDirectIndirectFlag = 0 // both direct and indirect
    // optionalDirectIndirectFlag = 1 // direct only
    let termTypeList = ['IN', 'PIN']
    new RxnavApi().json().rxclass().classMembers(classId, source, relationship, termTypeList, optionalDirectIndirectFlag)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassesClassMembers()

const drugClassesClassTree = () => {
    const classId = 'N0000185505' // Cytochrome P450 Inducers
    new RxnavApi().json().rxclass().classTree(classId)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))
}
// drugClassesClassTree()

const drugClassesSimilarInfo = () => {
    const drugClass1 = {
        classId: 'N02AA', // natural opium alkaloids
        relaSource: 'ATC',
        rela: null
    }
    const drugClass2 = {
        classId: 'N0000008072', // morphine derivatives
        relaSource: 'NDFRT',
        rela: 'has_ingredient'
    }

    new RxnavApi().json().rxclass().similarInfo(drugClass1, drugClass2)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesSimilarInfo()

const drugClassesSpellingSuggestions = () => {
    let term = 'marcolides'
    let type = 'CLASS' // 'DRUG' or 'CLASS'
    new RxnavApi().json().rxclass().spellingSuggestions(term, type)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesSpellingSuggestions()

const drugClassesSimilarByRxnormList = () => {
    let rxnormList = ['7052', '7676', '7804', '23088']
    let sourceOfDrugRelations = 'DAILYMED'
    let relationship = 'has_EPC'
    let scoreType = 0  // optional; 0=default
    let numberOfResultsToReturn = 5
    let equivalenceThreshold = null // 0.0-1.0
    let inclusionThreshold = null // 0.0-1.0 [only relevant for scoreType=1-or-2]
    const payload = {
        rxcuis: rxnormList,
        relaSource: sourceOfDrugRelations,
        rela: relationship,
        scoreType,
        top: numberOfResultsToReturn,
        equivalenceThreshold,
        inclusionThreshold
    }
    new RxnavApi().json().rxclass().similarByRxnormList(payload)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesSimilarByRxnormList()



const drugClassesSimilarToClass = () => {
    let classId = 'L01XE' // protein kinase inhibitors
    let sourceOfDrugRelations = 'ATC'
    let relationship = null //'has_EPC'
    let scoreType = 1  // optional; 0=default, 1=includes, 2=included_in
    let numberOfResultsToReturn = 2
    let equivalenceThreshold = null // 0.0-1.0
    let inclusionThreshold = null // 0.0-1.0 [only relevant for scoreType=1-or-2]

    const payload = {
        classId,
        relaSource: sourceOfDrugRelations,
        rela: relationship,
        scoreType,
        top: numberOfResultsToReturn,
        equivalenceThreshold,
        inclusionThreshold
    }
    new RxnavApi().json().rxclass().classesSimilarToClass(payload)
        .then((res) => console.log(JSON.stringify(res)))
        .catch(err => console.log(`err=${err}`))

}
// drugClassesSimilarToClass()
