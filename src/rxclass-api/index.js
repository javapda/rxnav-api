const axios = require('axios')
const _ = require('lodash')
module.exports = class RxclassApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `https://rxnav.nlm.nih.gov/REST/rxclass`
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    async classTypeList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/classTypes`,
            headers: this.headers()
        })).data

    }
    async byId(classId) {
        console.log(`${this.baseUrl()}/class/byId`)
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/class/byId`,
            params: { classId: classId },
            headers: this.headers()
        })).data

    }


    async byName(className, classTypeList) {
        const url = `${this.baseUrl()}/class/byName`
        const params = { className: className }
        if (classTypeList) {
            //url += "&classTypes=" + classTypeList.join("+")
            params.classTypes = classTypeList.join("+")
        }
        return (await axios({
            method: 'get',
            url,
            params,
            headers: this.headers()
        })).data

    }

    async sourceTypeList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/relaSources`,
            headers: this.headers()
        })).data
    }

    async relationTypeList(relaSourceList) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/relas`,
            params: { relaSource: relaSourceList.join('+') },
            headers: this.headers()
        })).data
    }

    async classesByRxnorm(rxnorm, optionalSourceList, optionalRelationshipList) {
        const params = {
            rxcui: rxnorm,
            relaSource: (optionalSourceList) ? optionalSourceList.join('+') : null,
            relas: (optionalRelationshipList) ? optionalRelationshipList.join('+') : null
        }
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/class/byRxcui`,
            params: params,
            headers: this.headers()
        })).data
    }

    async byDrugName(drugName, optionalSourceList, optionalRelationshipList) {
        const params = {
            drugName: drugName,
            relaSource: (optionalSourceList) ? optionalSourceList.join('+') : null,
            relas: (optionalRelationshipList) ? optionalRelationshipList.join('+') : null
        }
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/class/byDrugName`,
            params: params,
            headers: this.headers()
        })).data
    }

    async byClassTypeList(classTypeList) {
        const params = {
            classTypes: (classTypeList) ? classTypeList.join('+') : null
        }
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/allClasses`,
            params: params,
            headers: this.headers()
        })).data
    }

    /**
     * Get the class contexts. A context is a path from the root 
     * of the class hierarchy down to the specified class.
     * 
     * @param {*} classId 
     */
    async classContext(classId) {
        const params = {
            classId
        }
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/classContext`,
            params: params,
            headers: this.headers()
        })).data

    }

    async classGraph(classId, source) {
        const params = {
            classId, source
        }
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/classGraph`,
            params: params,
            headers: this.headers()
        })).data

    }

    async classMembers(classId, source, relationship, termTypeList = ['IN', 'PIN', 'MIN'], optionalDirectIndirectFlag = 0) {
        const params = {
            classId,
            relaSource: source,
            rela: relationship,
            trans: optionalDirectIndirectFlag
        }
        const url = `${this.baseUrl()}/classMembers?ttys=${termTypeList.join('+')}`
        return (await axios({
            method: 'get',
            url,
            params: params,
            headers: this.headers()
        })).data

    }
    async classTree(classId) {
        const url = `${this.baseUrl()}/classTree`
        return (await axios({
            method: 'get',
            url,
            params: { classId },
            headers: this.headers()
        })).data
    }

    async similarInfo(drugClass1, drugClass2) {
        const url = `${this.baseUrl()}/class/similarInfo`
        const params =
        {
            classId1: drugClass1.classId,
            relaSource1: drugClass1.relaSource,
            rela1: drugClass1.rela,
            classId2: drugClass2.classId,
            relaSource2: drugClass2.relaSource,
            rela2: drugClass2.rela
        }
        return (await axios({
            method: 'get',
            url,
            params: params,
            headers: this.headers()
        })).data
    }

    async spellingSuggestions(term, type) {
        const params = { term, type }
        const url = `${this.baseUrl()}/spellingsuggestions`
        return (await axios({
            method: 'get',
            url,
            params: params,
            headers: this.headers()
        })).data

    }

    /**
     * this is an odd one as the nih server seems to 
     * expect a space-separated list of rxnorm codes 
     * (vs. '+'-separated list in other API's)
     *  https://rxnav.nlm.nih.gov/RxClassAPIREST.html#label%3Afunctions=&uLink=RxClass_REST_findSimilarClassesByDrugList
     * @param {*} payload 
     */
    async similarByRxnormList(payload) {
        payload.rxcuis = payload.rxcuis.join(' ')
        const params = payload
        const url = `${this.baseUrl()}/class/similarByRxcuis`
        return (await axios({
            method: 'get',
            url,
            params: params,
            headers: this.headers()
        })).data
    }
    async classesSimilarToClass(payload) {
        const params = payload
        const url = `${this.baseUrl()}/class/similar`
        return (await axios({
            method: 'get',
            url,
            params: params,
            headers: this.headers()
        })).data
    }
}