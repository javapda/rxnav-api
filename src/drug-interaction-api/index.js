const axios = require('axios')
const _ = require('lodash')
module.exports = class DrugInteractionApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `https://rxnav.nlm.nih.gov/REST/interaction`
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    async version() {
        const url = `${this.baseUrl()}/version`
        return (await axios({
            method: 'get',
            url,
            headers: this.headers()
        })).data
    }

    async sources() {
        const url = `${this.baseUrl()}/sources`
        return (await axios({
            method: 'get',
            url,
            headers: this.headers()
        })).data
    }
    async interaction(rxnorm, sources) {
        const url = `${this.baseUrl()}/interaction`
        const params = { rxcui: rxnorm, sources: sources.join('+') }
        return (await axios({
            method: 'get',
            params,
            url,
            headers: this.headers()
        })).data
    }

    async interactionsBetweenDrugs(rxnorms, sources) {
        let url = `${this.baseUrl()}/list?rxcuis=${rxnorms.join('+')}`
        const params = { rxcuis: rxnorms.join('+') }
        if (sources) {
            url += `&values=${sources.join('+')}`
        }
        return (await axios({
            method: 'get',
            url,
            headers: this.headers()
        })).data
    }
}