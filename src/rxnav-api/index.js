const axios = require('axios')
const DrugInteractionApi = require('../drug-interaction-api')
const RxclassApi = require('../rxclass-api')
const RxnormApi = require('../rxnorm-api')
const RxtermsApi = require('../rxterms-api')
const RximageApi = require('../rximage-api')
module.exports = class RxnavApi {
    constructor() {
        this.config = {
            baseUrl: `https://rxnav.nlm.nih.gov/REST`,
            headers: {
                'X-Source': 'rxnav-api',
                'Accept': 'application/json'
            }
        }
    }
    isJson() {
        return this.config.headers.Accept === 'application/json'
    }
    isXml() {
        return !this.isJson()
    }
    json() {
        this.config.headers.Accept = 'application/json'
        return this
    }
    xml() {
        this.config.headers.Accept = 'application/xml'
        return this
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    drugInteractionApi() {
        return new DrugInteractionApi(this.config)
    }

    rxclass() {
        return new RxclassApi(this.config)
    }

    rximage() {
        return new RximageApi(this.config)
    }

    rxnorm() {
        return new RxnormApi(this.config)
    }

    rxterms() {
        return new RxtermsApi(this.config)
    }

    rxtermsSearchCriteria() {
        return RxtermsApi.createSearchCriteria()
    }

    async restfulEndpointsList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/`,
            headers: this.headers()
        })).data
    }

    async sourceTypeList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/sourcetypes`,
            headers: this.headers()
        })).data
    }

}