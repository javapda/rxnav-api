const axios = require('axios')
const RxcuiApi = require('./rxcui-api')

module.exports = class RxnormApi {
    constructor(config) {
        this.config = config
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    /**
     * ...allConceptsByTermTypeList(['BC','BPCK'])
     * @param {list of TTY codes} ttyList 
     */
    async allConceptsByTermTypeList(ttyList) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/allconcepts?tty=${ttyList.join('+')}`,
            headers: this.headers()
        })).data
    }

    async approximateTerm(payload) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/approximateTerm`,
            params: payload,
            headers: this.headers()
        })).data
    }

    /**
     * Get the brands that contain all the specified ingredients. 
     * Note that the brands returned may contain other ingredients in addition to those specified.
     * 
     * ingredientids - list of ingredient RxCUIs that the returned brands must contain.
     * @param {*} ingredientIdList 
     */
    async brandsContainingIngredients(ingredientIdList) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/brands?ingredientids=${ingredientIdList.join('+')}`,
            headers: this.headers()
        })).data
    }
    /**
     * Gets the names used by RxNav for auto completion. 
     * This is a large list which includes names of 
     * ingredients, brands, and branded packs.
     */
    async displayNames() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/displaynames`,
            headers: this.headers()
        })).data
    }

    /**
     * 
     * @param {name - an ingredient, brand, clinical dose form, branded dose form, clinical drug component or branded drug component name} name 
     */
    async drugs(name) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/drugs`,
            params: { name: name },
            headers: this.headers()
        })).data
    }

    /**
     * Get the valid identifier types of the RxNorm data set. 
     * @param {*} name 
     */
    async idTypeList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/idtypes`,
            headers: this.headers()
        })).data
    }

    /**
     * Get the National Drug Code (NDC) properties. This function returns the NDC properties for an RxNorm concept, an NDC or a structured product label. Only NDC properties of active NDCs curated by RxNorm are returned by this function. 
     * example ndc: 
     * NDC2: 67544-355
     * NDC3: 0781-1506-10, 	
     * NDC11: 00904629161
     * RxCUI: 213270
     * SPL_SET_ID: 1C5BC1DD-E9EC-44C1-9281-67AD482315D9
     * @param {id - the identifier. Valid identifiers are listed in the table below.}
     */
    async ndcProperties(ndc) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/ndcproperties`,
            params: { id: ndc },
            headers: this.headers()
        })).data
    }

    async ndcStatus(payload) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/ndcstatus`,
            params: payload,
            headers: this.headers()
        })).data
    }

    async propertyCategoryList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/propCategories`,
            headers: this.headers()
        })).data
    }

    async propertyNameList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/propnames`,
            headers: this.headers()
        })).data
    }

    async relationTypeList() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/relatypes`,
            headers: this.headers()
        })).data
    }

    /**
     * Get spelling suggestions for a given term. 
     * The suggestions are RxNorm terms contained in the current version, 
     * listed in decreasing order of closeness to the original phrase.
     * 
     * @param {the name for which spelling suggestions are to be generated. This field is required.} name 
     */
    async spellingSuggestionList(name) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/spellingsuggestions`,
            params: { name: name },
            headers: this.headers()
        })).data
    }

    /**
     * Get the valid term types in the RxNorm data set. 
     */
    async termTypes() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/termtypes`,
            headers: this.headers()
        })).data
    }

    /**
     * Get the version of the RxNorm data set.
     */
    async version() {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/version`,
            headers: this.headers()
        })).data
    }

    rxcui() {
        return new RxcuiApi(this.config)
    }

    doesRxnormHaveProperty(rxnorm, propertyInfo) {
        return this.rxcui().doesRxnormHaveProperty({ rxnorm: rxnorm, propertyInfo: propertyInfo })
    }
    rxnormRelatedConceptList(rxnorm) {
        return this.rxcui().rxnormRelatedConceptList(rxnorm)
    }
    rxnormRelatedConceptListByRelationalAttributes(rxnorm, relationalAttributes) {
        return this.rxcui().rxnormRelatedConceptListByRelationalAttributes(rxnorm, relationalAttributes)
    }
    rxnormRelatedConceptListByTermTypes(rxnorm, termTypes) {
        return this.rxcui().rxnormRelatedConceptListByTermTypes(rxnorm, termTypes)
    }


    /**
     * 
     * @param {*} termType 
     * @param {*} otherVocabularyCode 
     * @param {*} allsrc 
     */
    async rxnormsForOtherVocabulary(termType, otherVocabularyCode, allsrc = 0) {
        let res = (await this.rxcui().rxnormsForOtherVocabulary(
            {
                idtype: termType,
                id: otherVocabularyCode,
                allsrc: allsrc
            }
        ))
        res = res.idGroup
        return (res && res.rxnormId) ? res.rxnormId : null
    }

    /**
     * Given a First Databank gcnseqno return a list of rxnorm ids
     * 
     * @param {*} gcnSeqno 
     * @param {*} allsrc 
     */
    rxnormsForFdbGcnSeqno(gcnSeqno, allsrc = 1) {
        return this.rxnormsForOtherVocabulary('GCN_SEQNO', gcnSeqno, allsrc)
    }

    rxnormsForFdbIngredientId(fdbIngredientId, allsrc = 1) {
        return this.rxnormsForOtherVocabulary('HIC_SEQN', fdbIngredientId, allsrc)
    }

    rxnormsForHcpcsCode(hcpcsCode, allsrc = 1) {
        return this.rxnormsForOtherVocabulary('HCPCS', hcpcsCode, allsrc)
    }

    rxnormsForSnomedCode(snomedCode, allsrc = 1) {
        return this.rxnormsForOtherVocabulary('SNOMEDCT', snomedCode, allsrc)
    }
    rxnormsForNdc(ndc, allsrc = 1) {
        return this.rxnormsForOtherVocabulary('NDC', ndc, allsrc)
    }

    rxnormStatus(rxnorm) {
        return this.rxcui().rxnormStatus(rxnorm)
    }

    rxnormProperties(rxnorm) {
        return this.rxcui().rxnormProperties(rxnorm)
    }

    rxnormProperty(rxnorm, property) {
        return this.rxcui().rxnormProperty(rxnorm, { propName: property })
    }

    async rxnormNdcList(rxnorm) {
        console.log(JSON.stringify((await this.rxcui().rxnormNdcList(rxnorm))))
        let res = (await this.rxcui().rxnormNdcList(rxnorm)).ndcGroup
        return (res && res.ndcList) ? res.ndcList : null
    }
    rxnormProprietaryInfo(rxnorm, srclist = [], serviceTicket) {
        return this.rxcui().rxnormProprietaryInfo({ rxnorm: rxnorm, srclist: srclist, ticket: serviceTicket })
    }

    allRelated(rxnorm) {
        return this.rxcui().rxnormProprietaryInfo({ rxnorm: rxnorm, srclist: srclist, ticket: serviceTicket })
    }
}
