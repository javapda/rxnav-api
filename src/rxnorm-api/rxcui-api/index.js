const axios = require('axios')
const _ = require('lodash')
module.exports = class RxcuiApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `https://rxnav.nlm.nih.gov/REST/rxcui`
    }
    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    async doesRxnormHaveProperty(payload) {
        let rxnorm = payload.rxnorm
        let params = payload.propertyInfo
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/filter`,
            params: params,
            headers: this.headers()
        })).data

    }

    /**
     * history - (optional) if the value is 2 or not specified, all NDCs, 
     * past or present, are returned. 
     * 
     * This includes NDCs associated with concepts that were remapped into 
     * the specified concept.
     * 
     * A value of 1 indicates all NDCs, past or present, but not including any 
     * remapped concept NDCs.
     * 
     * A value of 0 indicates only currently associated NDCs with the concept 
     * will be returned. Only the NDCs curated by RxNorm (i.e., with SAB = RXNORM) 
     * are returned by this function (i.e., the NDCs provided solely by proprietary 
     * sources, but not curated by RxNorm are omitted).
     * 
     * Currently associated NDCs are those NDCs present in the current version 
     * of RxNorm.
     * 
     * Past NDCs include all NDCs ever mentioned in any version of RxNorm, 
     * regardless of presence in the current version.
     * 
     * @param {rxnorm code} rxnorm 
     */
    async ndcsForRxnorm(rxnorm, historyCode) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/allhistoricalndcs${historyCode ? '?history=' + historyCode : ''}`,
            headers: this.headers()
        })).data

    }

    /**
     * only seems to support json
     * 
     * @param {*} payload 
     */
    async rxnormsForOtherVocabulary(payload) {
        let localConfig = _.clone(this.config)
        localConfig.headers.Accept = 'application/json'
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}`,
            params: payload,
            headers: localConfig.headers
        })).data
    }
    async rxnormStatus(rxnorm) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/status`,
            headers: this.headers()
        })).data
    }
    async rxnormProperties(rxnorm) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/properties`,
            headers: this.headers()
        })).data
    }
    async rxnormProperty(rxnorm, params) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/property`,
            params: params,
            headers: this.headers()
        })).data
    }
    /**
     * NOTE: only available in json
     */
    async rxnormNdcList(rxnorm) {
        let localConfig = _.clone(this.config)
        localConfig.headers.Accept = 'application/json'
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/ndcs`,
            headers: localConfig.headers
        })).data
    }

    /**
     * retrieve proprietary info about an rxnorm
     * @param {*} payload 
     */
    async rxnormProprietaryInfo(payload) {
        //payload.rxaui = "?"
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${payload.rxnorm}/proprietary`,
            params: payload,
            headers: this.headers()
        })).data
    }

    async rxnormRelatedConceptList(rxnorm) {
        return (await axios({
            method: 'get',
            url: `${this.baseUrl()}/${rxnorm}/allrelated`,
            headers: this.headers()
        })).data

    }

    /**
     * 
     * @param {*} rxnorm 
     * @param  {...any} relationalAttributes 
     */
    async rxnormRelatedConceptListByRelationalAttributes(rxnorm, relationalAttributes) {
        // TODO: verify valid relationalAttributes provided
        let url = `${this.baseUrl()}/${rxnorm}/related?rela=${relationalAttributes.join('+')}`
        return (await axios({
            method: 'get',
            url: url,
            headers: this.headers()
        })).data
    }

    /**
     * 
     * @param {*} rxnorm 
     * @param {*} termTypes 
     */
    async rxnormRelatedConceptListByTermTypes(rxnorm, termTypes) {
        let url = `${this.baseUrl()}/${rxnorm}/related?tty=${termTypes.join('+')}`
        return (await axios({
            method: 'get',
            url: url,
            headers: this.headers()
        })).data

    }

}
