const axios = require('axios')
const _ = require('lodash')

module.exports = class RxtermsApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `https://rxnav.nlm.nih.gov/REST/RxTerms`
    }

    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }

    async version() {
        return (await axios(
            {
                method: 'get',
                url: `${this.baseUrl()}/version`,
                headers: this.headers()
            }
        )).data
    }

    async allConcepts() {
        return (await axios(
            {
                method: 'get',
                url: `${this.baseUrl()}/allconcepts`,
                headers: this.headers()
            }
        )).data
    }

    /**
     * Get the RxTerms display name for a specified RxNorm concept.
     * 
     * @param {*} rxnorm 
     */
    async rxnormDisplayName(rxnorm) {
        return (await axios(
            {
                method: 'get',
                url: `${this.baseUrl()}/rxcui/${rxnorm}/name`,
                headers: this.headers()
            }
        )).data
    }

    async rxnormTerms(rxnorm) {
        return (await axios(
            {
                method: 'get',
                url: `${this.baseUrl()}/rxcui/${rxnorm}/allinfo`,
                headers: this.headers()
            }
        )).data
    }
}
