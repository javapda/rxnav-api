const axios = require('axios')
const _ = require('lodash')
const RxtermsSearchCriteria = require('./rximage-searchCriteria')
const validDrugShapes = [
    'BULLET',
    'CAPSULE',
'CLOVER',
'DIAMOND',
'DOUBLE CIRCLE',
'FREEFORM',
'GEAR',
'HEPTAGON',
'HEXAGON',
'OCTAGON',
'OVAL',
'PENTAGON',
'RECTANGLE',
'ROUND',
'SEMI-CIRCLE',
'SQUARE',
'TEAR',
'TRAPEZOID',
'TRIANGLE',
]

/**
 * https://rxnav.nlm.nih.gov/RxImageAPIParameters.html
 */
module.exports = class RximageApi {
    constructor(config) {
        this.config = _.clone(config)
        this.config.baseUrl = `https://rximage.nlm.nih.gov/api/rximage/1`
    }

    baseUrl() {
        return this.config.baseUrl
    }
    headers() {
        return this.config.headers
    }
    createSearchCriteria() {
        return new RxtermsSearchCriteria()
    }
    async search(searchCriteria, resourcePath='rxnav') {
        const url = `${this.baseUrl()}/${resourcePath}`
        // throw JSON.stringify(searchCriteria)
        return (await axios({
            method:'get',
            url,
            params:searchCriteria,
            headers: this.headers()
        })).data
    }

}