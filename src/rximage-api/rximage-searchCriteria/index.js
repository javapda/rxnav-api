/**
 * 
 * 
color	Search by pill color.
id	Search by image id.
imprint	Search by physical imprint present on pill.
imprintColor	Search by the color of the imprint.
imprintType	Search by the type of imprint.
inactive	Search by name of inactive ingredient.
matchPackSize	Filter by pack size when searching by NDC.
matchRelabeled	Include relabeled versions when searching by NDC.
name	Search by drug name.
ndc	Search by NDC.
parse	Search using a string with multiple arguments.
rootId	Search by SPL Root ID.
rxcui	Search by RxCUI.
score	Search by number of segments the pill is scored to split into.
setId	Search by SPL Set ID.
shape	Search by pill shape.
size	Search by pill size in millimeters.
sizeT	Set size tolerance when searching by size.
symbol
 */
module.exports = class RximageSearchCriteria {
    constructor() {
        this._payload = {
            color: null, // search by pill color
            id: null, // search by image id
            imprint: null, // search by physical imprint present on pill
            imprintColor: null, // search by the color of the imprint
            imprintType: null, // search by the type of imprint
            inactive: null, // search by name of inactive ingredient
            matchPackSize: null, // filter by pack size when searching by NDC
            matchRelabeled: null, // include relabeled versions when searching by NDC
            name: null, //	search by drug name
            ndc: null, //	search by NDC
            parse: null, // search using a string with multiple arguments
            rootId: null, // search by SPL Root ID
            rxcui: null, // search by RxCUI
            score: null, // search by number of segments the pill is scored to split into
            setId: null, // search by SPL Set ID
            shape: null, // search by pill shape
            size: null, //	search by pill size in millimeters
            sizeT: null, // set size tolerance when searching by size
            symbol: null, // include or exclude pills with symbols or markings

            includeActive: false, // include data on active ingredients in reply
            includeInactive: false, // include data on inactive ingredients in reply.
            includeIngredients: false, // include data on active and inactive ingredients in reply.
            includeMpc: false, // include MPC data in reply
            resolution: 'full', // specify resolution of images returned (full,120,300,600,800,1024)
            rLimit: null, // limit total number of results returned
            rPage: null, // specify which page of results to return (1..mz)
            rPageSize: null // set number of results per page
        }
    }

    color(color) {
        this._payload.color = color
        return this
    }
    id(id) {
        this._payload.id = id
        return this
    }
    imprint(imprint) {
        this._payload.imprint = imprint
        return this
    }
    imprintColor(imprintColor) {
        this._payload.imprintColor = imprintColor
        return this
    }
    imprintType(imprintType) {
        this._payload.imprintType = imprintType
        return this
    }
    inactive(inactive) {
        this._payload.inactive = inactive
        return this
    }
    includeActive(includeActive) {
        this._payload.includeActive = includeActive
        return this
    }
    includeInactive(includeInactive) {
        this._payload.includeInactive = includeInactive
        return this
    }
    includeIngredients(includeIngredients) {
        this._payload.includeIngredients = includeIngredients
        return this
    }
    includeMpc(includeMpc) {
        this._payload.includeMpc = includeMpc
        return this
    }
    limitNumberOfRecords(limitNumberOfRecords) {
        this._payload.rLimit = limitNumberOfRecords
        return this
    }
    matchPackSize(matchPackSize) {
        this._payload.matchPackSize = matchPackSize
        return this
    }
    matchRelabeled(matchRelabeled) {
        this._payload.matchRelabeled = matchRelabeled
        return this
    }
    name(name) {
        this._payload.name = name
        return this
    }
    ndc(ndc) {
        this._payload.ndc = ndc
        return this
    }
    pageNumber(pageNumber) {
        this._payload.rPage = pageNumber
        return this
    }
    pageSize(pageSize) {
        this._payload.rPageSize = pageSize
        return this
    }
    resolution(resolution) {
        this._payload.resolution = resolution
        return this
 
    }
    /**
     * '+' separated string of terms
     * e.g. blue+round
     * 
     * @param {*} parse 
     */
    parse(parse) {
        this._payload.parse = parse
        return this
    }
    rootId(rootId) {
        this._payload.rootId = rootId
        return this
    }
    rxnorm(rxcui) {
        this._payload.rxcui = rxcui
        return this
    }
    /**
     * Search by number of segments the pill is scored, 
     * or designed to split into. 
     * 
     * @param {*} score 
     */
    score(score) {
        this._payload.score = score
        return this
    }
    setId(setId) {
        this._payload.setId = setId
        return this
    }
    shape(shape) {
        this._payload.shape = shape
        return this
    }
    size(size) {
        this._payload.size = size
        return this
    }
    sizeTolerance(sizeTolerance) {
        this._payload.sizeT = sizeTolerance
        return this
    }
    symbol(symbol) {
        this._payload.symbol = symbol
        return this
    }


    params() {
        return this._payload
    }
}